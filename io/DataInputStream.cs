﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace NBELICS.io
{
    public class DataInputStreamReader
    {
        private Stream _in;
        private byte[] _b = new byte[500];

        public long Position { get { return _in.Position; } set { _in.Position = value; } }

        public DataInputStreamReader(Stream s)
        {
            _in = s;
            if(!s.CanRead || !s.CanSeek)
                throw new InvalidOperationException("The stream provided cannot be read or seeked.");
        }

        public byte[] ReadBytes(int len)
        {
            int off = 0, r = 0;
            do
            {
                r = _in.Read(_b, off, len - off);
                if (r != -1)
                    off += r;
            } while (r > 0 && off != len);
            return _b;
        }

        public int Read(byte[] d, int off, int len)
        {
            return _in.Read(d, off, len);
        }

        public void ReadFully(byte[]d, int off, int len)
        {
            var end = off + len;
            for (var o = off; (o += _in.Read(d, o, end - o)) < end; ) ;
        }

        public sbyte ReadByte()
        {
            return (sbyte) ReadBytes(1)[0];
        }

        public byte ReadUByte()
        {
            return ReadBytes(1)[0];
        }

        public short ReadShort()
        {
            var data = ReadBytes(2);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(_b, 0, 2);
            return BitConverter.ToInt16(data, 0);
        }

        public ushort ReadUShort()
        {
            var data = ReadBytes(2);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(_b, 0, 2);
            return BitConverter.ToUInt16(data, 0);
        }

        public uint ReadUInt()
        {
            var data = ReadBytes(4);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(_b, 0, 4);
            return BitConverter.ToUInt32(data, 0);
        }

        public int ReadInt()
        {
            var data = ReadBytes(4);
            if(BitConverter.IsLittleEndian)
                Array.Reverse(_b, 0, 4);
            return BitConverter.ToInt32(data, 0);
        }

        public float ReadFloat()
        {
            var data = ReadBytes(4);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(_b, 0, 4);
            return BitConverter.ToSingle(data, 0);
        }

        public long ReadLong()
        {
            var data = ReadBytes(8);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(_b, 0, 8);
            return BitConverter.ToInt64(data, 0);
        }

        public ulong ReadULong()
        {
            var data = ReadBytes(8);
            if(BitConverter.IsLittleEndian)
                Array.Reverse(_b, 0, 8);
            return BitConverter.ToUInt64(data, 0);
        }

        public double ReadDouble()
        {
            var data = ReadBytes(8);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(_b, 0, 8);
            return BitConverter.ToDouble(data, 0);
        }

        public string ReadUTF()
        {
            var length = ReadUShort();
            var i = 0;
            var sb = new StringBuilder();

            while (i < length) {
                 var b = ReadUByte();
                 if (b > 127)
                     break;
                 i++;
                sb.Append((char) b);
            }

            while(i < length)
            {
                i++;
                var b = ReadUByte();

                switch((b >> 4) & 0xF)
                {
                    case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                         /* 0xxxxxxx*/
                         i++;
                        sb.Append((char) b);
                        break;

                        case 12: case 13:
                         /* 110x xxxx   10xx xxxx*/
                         i += 2;
                         if (i > length)
                             throw new DataException("malformed input: partial character at end");
                         var c = ReadUByte();
                         if ((c & 0xC0) != 0x80)
                             throw new DataException("malformed input around byte " + i);
                         sb.Append((char)(((c & 0x1F) << 6) | (c & 0x3F)));
                        break;

                        case 14:
                         /* 1110 xxxx  10xx xxxx  10xx xxxx */
                        i += 3;
                         if (i > length)
                             throw new DataException("malformed input: partial character at end");
                         c = ReadUByte();
                         var d = ReadUByte();
                         if (((c & 0xC0) != 0x80) || ((d & 0xC0) != 0x80))
                             throw new DataException("malformed input around byte " + (i-1));
                         sb.Append((char)(((c     & 0x0F) << 12) | ((c & 0x3F) << 6)  | ((d & 0x3F) << 0)));
                         break;
                     default:
                         /* 10xx xxxx,  1111 xxxx */
                         throw new DataException("malformed input around byte " + i);
                }
            }

            return sb.ToString();
        }
    }
}