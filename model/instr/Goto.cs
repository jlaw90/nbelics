﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Goto: BranchInstruction
    {
        public override byte Opcode
        {
            get
            {
                return (byte)(IsWide ? 0xc8 : 0xa7);
            }
        }
        public override uint Size
        {
            get
            {
                return (byte) (IsWide? 5: 3);
            }
        }

        public bool IsWide { get {
            var dif = Branch.Location - Location;
            return dif < short.MinValue || dif > short.MaxValue;
        }}

        public Goto(Instruction branch)
        {
            Branch = branch;
        }

        internal Goto(int _b)
        {
            _branch = _b;
        }
    }
}
