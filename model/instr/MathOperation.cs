﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class MathOperation : Instruction
    {
        public enum Operator
        {
            Add,
            Subtract,
            Multiply,
            Divide,
            Remainder
        }

        public override byte Opcode
        {
            get
            {
                switch (Operation)
                {
                    case Operator.Add:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x60; // iadd
                            case JavaType.Long:
                                return 0x61; // ladd
                            case JavaType.Float:
                                return 0x62; // fadd
                            case JavaType.Double:
                                return 0x63; // dadd
                        }
                        break;
                    case Operator.Subtract:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x64; // isub
                            case JavaType.Long:
                                return 0x65; // lsub
                            case JavaType.Float:
                                return 0x66; // fsub
                            case JavaType.Double:
                                return 0x67; // dsub
                        }
                        break;
                    case Operator.Multiply:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x68; // imul
                            case JavaType.Long:
                                return 0x69; // lmul
                            case JavaType.Float:
                                return 0x6a; // fmul
                            case JavaType.Double:
                                return 0x6b; // dmul
                        }
                        break;
                    case Operator.Divide:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x6c; // idiv
                            case JavaType.Long:
                                return 0x6d; // ldiv
                            case JavaType.Float:
                                return 0x6e; // fdiv
                            case JavaType.Double:
                                return 0x6f; // ddiv
                        }
                        break;
                    case Operator.Remainder:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x70; // irem
                            case JavaType.Long:
                                return 0x71; // lrem
                            case JavaType.Float:
                                return 0x72; // frem
                            case JavaType.Double:
                                return 0x73; // drem
                        }
                        break;
                }
                throw new InvalidDataException();
            }
        }
        public override ushort StackProduced
        {
            get { return (ushort)(Type == JavaType.Double || Type == JavaType.Long ? 2 : 1); }
        }
        public override ushort StackConsumed
        {
            get
            {
                return (ushort) (Type == JavaType.Double || Type == JavaType.Long? 4: 2);
            }
        }
        public Operator Operation { get; set; }
        private JavaType _type;
        public JavaType Type
        {
            get { return _type; }
            set
            {
                if (value != JavaType.Integer && value != JavaType.Long && value != JavaType.Float && value != JavaType.Double)
                    throw new InvalidDataException("Valid types for math operations are int, float, long or double");
                _type = value;
            }
        }

        public MathOperation(JavaType type, Operator o)
        {
            Operation = o;
            Type = type;
        }
    }
}