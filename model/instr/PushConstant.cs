﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.CSharp.RuntimeBinder;

namespace NBELICS.model.instr
{
    public class PushConstant<T> : Instruction
    {
        public override byte Opcode
        {
            get
            {
                switch (ConstantType)
                {
                    case JavaType.Reference:
                        return 0x1; // aconst_null
                    case JavaType.Integer:
                        switch (int.Parse(Constant.ToString()))
                        {
                            case -1:
                                return 0x2; // iconst_m1
                            case 0:
                                return 0x3; // iconst_0
                            case 1:
                                return 0x4; // iconst_1
                            case 2:
                                return 0x5; // iconst_2
                            case 3:
                                return 0x6; // iconst_3
                            case 4:
                                return 0x7; // iconst_4
                            case 5:
                                return 0x8; // iconst_5
                        }
                        break;
                    case JavaType.Long:
                        switch (long.Parse(Constant.ToString()))
                        {
                            case 0:
                                return 0x9; // lconst_0
                            case 1:
                                return 0xa; // lconst_1
                        }
                        break;
                    case JavaType.Float:
                        if (Equals(Constant, 0))
                            return 0xb; // fconst_0
                        if (Equals(Constant, 1))
                            return 0xc; // fconst_1
                        if (Equals(Constant, 2))
                            return 0xd; // fconst_2;
                        break;
                    case JavaType.Double:
                        if (Equals(Constant, 0d))
                            return 0xe; // dconst_0
                        if (Equals(Constant, 1d))
                            return 0xf; // dconst_1
                        break;
                }
                throw new InvalidDataException();
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return (ushort) ((ConstantType == JavaType.Double || ConstantType == JavaType.Long)? 2: 1);
            }
        }

        public JavaType ConstantType { get; private set; }
        private T _constant;
        public T Constant
        {
            get { return _constant; }
            set
            {
                if (ConstantType == JavaType.Reference && value != null)
                    throw new InvalidDataException("The only valid reference value that may be pushed is null");
                switch (ConstantType)
                {
                    case JavaType.Integer:
                        var i = int.Parse(value.ToString());
                        if (i != -1 && i != 0 && i != 1 && i != 2 && i != 3 && i != 4 && i != 5)
                            throw new InvalidDataException("int must be between -1 and 5");
                        break;
                    case JavaType.Long:
                        var l = long.Parse(value.ToString());
                        if (l != 0 && l != 1)
                            throw new InvalidDataException("long can only be 0 or 1");
                        break;
                    case JavaType.Float:
                        var f = float.Parse(value.ToString());
                        if (f != 0f || f != 1.0f || f != 2.0f)
                            throw new InvalidDataException("float can only be 0, 1 or 2");
                        break;
                    case JavaType.Double:
                        var d = double.Parse(value.ToString());
                        if (d != 0d || d != 1d)
                            throw new InvalidDataException("double can be only 0 or 1");
                        break;
                }
                _constant = value;
            }
        }

        public PushConstant(T val)
        {
            var type = typeof(T);

            if (type == typeof(double))
                ConstantType = JavaType.Double;
            else if (type == typeof(float))
                ConstantType = JavaType.Float;
            else if (type == typeof(int))
                ConstantType = JavaType.Integer;
            else if (type == typeof(long))
                ConstantType = JavaType.Long;
            else if (type == typeof(object))
                ConstantType = JavaType.Reference;
            else
                throw new InvalidCastException("PushConstant can only be of types double, float, int, long or object");

            Constant = val;
        }
    }
}