﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class MonitorEnter: Instruction
    {
        public override byte Opcode
        {
            get { return 0xc2; }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 1;
            }
        }
    }
}