﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class PutField: Instruction
    {
        public override byte Opcode
        {
            get { return 0xb5; }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }
        // Todo: calculate consumed stack (1 + field type...)
        public override ushort StackConsumed
        {
            get
            {
                return base.StackConsumed;
            }
        }

        public ConstantFieldRef Field { get; set; }

        public PutField(ConstantFieldRef f)
        {
            Field = f;
        }
    }
}