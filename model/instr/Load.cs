﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Load : Instruction, IWideInstruction
    {
        public override byte Opcode
        {
            get
            {
                switch (LoadType)
                {
                    case JavaType.Byte:
                    case JavaType.Boolean:
                    case JavaType.Character:
                    case JavaType.Short:
                    case JavaType.Integer:
                        switch (LoadIndex)
                        {
                            case 0:
                                return 0x1a;
                            case 1:
                                return 0x1b;
                            case 2:
                                return 0x1c;
                            case 3:
                                return 0x1d;
                            default:
                                return 0x15;
                        }
                    case JavaType.Long:
                        switch (LoadIndex)
                        {
                            case 0:
                                return 0x1e;
                            case 1:
                                return 0x1f;
                            case 2:
                                return 0x20;
                            case 3:
                                return 0x21;
                            default:
                                return 0x16;
                        }
                    case JavaType.Float:
                        switch (LoadIndex)
                        {
                            case 0:
                                return 0x22;
                            case 1:
                                return 0x23;
                            case 2:
                                return 0x24;
                            case 3:
                                return 0x25;
                            default:
                                return 0x17;
                        }
                    case JavaType.Double:
                        switch (LoadIndex)
                        {
                            case 0:
                                return 0x26;
                            case 1:
                                return 0x27;
                            case 2:
                                return 0x28;
                            case 3:
                                return 0x29;
                            default:
                                return 0x18;
                        }
                    case JavaType.Reference:
                        switch (LoadIndex)
                        {
                            case 0:
                                return 0x2a;
                            case 1:
                                return 0x2b;
                            case 2:
                                return 0x2c;
                            case 3:
                                return 0x2d;
                            default:
                                return 0x19;
                        }
                }
                throw new InvalidEnumArgumentException("LoadType is not set to a value");
            }
        }

        public override uint Size
        {
            get
            {
                return (byte)(1 + (LoadIndex > 3 ? 1 : 0) + (IsWide ? 1 : 0));
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return (ushort) ((LoadType == JavaType.Double || LoadType == JavaType.Long) ? 2 : 1);
            }
        }
        public bool IsWide { get { return LoadIndex > 255; } }
        public JavaType LoadType { get; set; }
        public ushort LoadIndex { get; set; }

        public Load(JavaType t, ushort idx)
        {
            LoadType = t;
            LoadIndex = idx;
        }
    }
}