﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class InvokeVirtual : Instruction
    {
        public override byte Opcode
        {
            get { return 0xb6; }
        }
        // Todo: calculate
        public override ushort StackConsumed
        {
            get
            {
                return base.StackConsumed;
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return base.StackProduced;
            }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }

        public ConstantMethodRef Method { get; set; }

        public InvokeVirtual(ConstantMethodRef cmr)
        {
            Method = cmr;
        }
    }
}