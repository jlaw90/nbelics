﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Return : Instruction
    {
        public override byte Opcode
        {
            get
            {
                switch(Type)
                {
                    case JavaType.Integer:
                        return 0xac; // ireturn
                        case JavaType.Long:
                        return 0xad; // lreturn
                    case JavaType.Float:
                        return 0xae; // freturn
                    case JavaType.Double:
                        return 0xaf; // dreturn
                        case JavaType.Reference:
                        return 0xb0; // areturn
                    case null:
                        return 0xb1; // return
                }
                throw new InvalidDataException();
            }
        }
        public override ushort StackConsumed
        {
            get { return (ushort) (Type == JavaType.Reference? 0: (Type == JavaType.Long || Type == JavaType.Double)? 2: 1); }
        }
        private JavaType? _type;
        public JavaType? Type
        {
            get { return _type; }
            set
            {
                if(value != null && value != JavaType.Integer && value != JavaType.Long && value != JavaType.Float &&
                    value != JavaType.Double && value != JavaType.Reference)
                    throw new InvalidDataException("Return can only return null, int, long, float, double or reference types");
                _type = value;
            }
        }

        public Return(JavaType? t)
        {
            Type = t;
        }
    }
}

