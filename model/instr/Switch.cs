﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Switch: BranchInstruction
    {
        public override byte Opcode { get { return (byte) (IsContinuous ? 0xaa : 0xab); } }
        public override uint Size { get
        {
            if(IsContinuous) // TABLESWITCH
            {
                return (uint) (13 + Padding + (JumpTable.Count * 4));
            }
            // LOOKUPSWITCH
            return (uint) (9 + Padding + (JumpTable.Count * 8));
        }}
        public override ushort StackConsumed { get { return 1; } }
        public SortedDictionary<int, Instruction> JumpTable { get; private set; }
        public bool IsContinuous
        {
            get
            {
                var en = JumpTable.GetEnumerator();
                if (!en.MoveNext())
                    return true; // 0 entries
                var e = en.Current;
                var l = e.Key - 1;
                do
                {
                    e = en.Current;
                    if (e.Key != l + 1)
                        return false;
                    l = e.Key;
                } while (en.MoveNext());
                return true;
            }
        }
        public byte Padding { get { return (byte) (4 - (Location % 4)); } }

        public Instruction this[int i]
        {
            get { return JumpTable[i]; }
            set { JumpTable[i] = value; }
        }

        internal Dictionary<int, int> _jumpTable; // Used before bytecode reconstitution is completed  

        public Switch(Instruction def)
        {
            JumpTable = new SortedDictionary<int, Instruction>();
            Branch = def;
        }

        internal Switch(int def, Dictionary<int, int> jumpTable)
        {
            _branch = def;
            _jumpTable = jumpTable;
            JumpTable = new SortedDictionary<int, Instruction>();
        }
    }
}