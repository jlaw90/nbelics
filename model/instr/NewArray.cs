﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class NewArray: Instruction
    {
        public enum ArrayType
        {
            Boolean = 4,
            Character,
            Float,
            Double,
            Byte,
            Short,
            Int,
            Long
        }

        public override byte Opcode
        {
            get { return 0xbc; }
        }
        public override uint Size
        {
            get
            {
                return 2;
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 1;
            }
        }
        public override ushort StackProduced
        {
            get { return 1; }
        }

        public ArrayType Type { get; set; }

        public NewArray(ArrayType t)
        {
            Type = t;
        }
    }
}