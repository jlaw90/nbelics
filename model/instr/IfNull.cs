﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class IfNull : BranchInstruction
    {
        public override byte Opcode
        {
            get { return 0xc6; }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 1;
            }
        }

        public IfNull(Instruction branch)
        {
            Branch = branch;
        }

        internal IfNull(short _branch)
        {
            this._branch = _branch;
        }
    }
}