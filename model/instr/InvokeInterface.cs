﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class InvokeInterface: Instruction
    {
        public override byte Opcode
        {
            get { return 0xb9; }
        }
        public override uint Size
        {
            get
            {
                return 5;
            }
        }
        // Todo: calculate stack produced and consumed
        public override ushort StackConsumed
        {
            get
            {
                return base.StackConsumed;
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return base.StackProduced;
            }
        }
        // Todo: calculate with descriptor
        public byte Count { get; private set; }

        public ConstantInterfaceMethodRef Method { get; set; }

        public InvokeInterface(ConstantInterfaceMethodRef cimr)
        {
            Method = cimr;
        }

        internal InvokeInterface(ConstantInterfaceMethodRef cimr, byte count, byte discard): this(cimr)
        {
            Count = count;
        }
    }
}