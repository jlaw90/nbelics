﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class AThrow: Instruction
    {
        public override byte Opcode
        {
            get { return 0xbf; }
        }
        public override ushort StackConsumed
        {
            get { return 1; }
        }
    }
}
