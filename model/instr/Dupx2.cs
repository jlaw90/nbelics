﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Dupx2: Instruction
    {
        public override byte Opcode
        {
            get { return 0x5b; }
        }
        public override ushort StackProduced
        {
            get
            {
                return 4;
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 3;
            }
        }
    }
}
