﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class ArrayStore: Instruction
    {
        public override byte Opcode
        {
            get
            {
                switch (ArrayType)
                {
                    case JavaType.Integer:
                        return 0x4f;
                    case JavaType.Long:
                        return 0x50;
                    case JavaType.Float:
                        return 0x51;
                    case JavaType.Double:
                        return 0x52;
                    case JavaType.Reference:
                        return 0x53;
                    case JavaType.Boolean:
                    case JavaType.Byte:
                        return 0x54;
                    case JavaType.Character:
                        return 0x55;
                    case JavaType.Short:
                        return 0x56;
                }
                throw new InvalidOperationException();
            }
        }

        public override ushort StackConsumed
        {
            get { return (ushort) (ArrayType == JavaType.Long || ArrayType == JavaType.Double? 4: 3); }
        }
        public JavaType ArrayType { get; set; }

        public ArrayStore(JavaType aType)
        {
            ArrayType = aType;
        }
    }
}
