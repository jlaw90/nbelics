﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Push: Instruction
    {
        public short Value { get; set; }

        public override byte Opcode
        {
            get { return (byte) (IsShortPush ? 0x11 : 0x10); }
        }

        public override uint Size
        {
            get { return (byte)(IsShortPush ? 0x03 : 0x02); }
        }

        public bool IsShortPush { get { return Value < SByte.MinValue || Value > SByte.MaxValue; } }

        public ushort StackAdd
        {
            get { return 1; }
        }

        public ushort StackRemove
        {
            get { return 0; }
        }

        public Push(short p)
        {
            Value = p;
        }
    }
}
