﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public abstract class BranchInstruction: Instruction
    {
        public Instruction Branch { get; set; }
        internal int _branch;
    }
}