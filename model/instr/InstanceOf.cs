﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class InstanceOf: Instruction
    {
        public override byte Opcode
        {
            get { return 0xc1; }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 1;
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return 1;
            }
        }

        public ConstantClass Class { get; set; }

        public InstanceOf(ConstantClass c)
        {
            Class = c;
        }
    }
}