﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class GetStatic: Instruction
    {
        public override byte Opcode
        {
            get { return 0xb2; }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }
        public override ushort StackProduced
        {
            get
            {
                // Todo: calculate whether field is double or long and modify accordingly
                throw new NotImplementedException();
            }
        }
        public ConstantFieldRef Field { get; set; }

        public GetStatic(ConstantFieldRef fr)
        {
            Field = fr;
        }
    }
}