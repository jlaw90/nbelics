﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public abstract class Instruction
    {
        public abstract byte Opcode { get; }
        public virtual uint Size { get { return 1; } }
        public Instruction Next { get; internal set; }
        public Instruction Previous { get; internal set; }
        public virtual ushort StackProduced { get { return 0; } }
        public virtual ushort StackConsumed { get { return 0; } }
        public uint Location
        {
            get
            {
                // Calculate the location of this instruction in the code array...
                // Used when re-generating code

                // Get start instruction
                var start = this;
                while (start.Previous != null)
                    start = start.Previous;

                // Calculate offset to here...
                uint i;
                for (i = 0; start != this; start = start.Next)
                    i += start.Size;
                return i;
            }
        }

        public void ReplaceWith(Instruction i)
        {
            InsertBefore(i);
            Remove();
        }

        public void Remove()
        {
            if(Next != null)
                Next.Previous = Previous;
            if(Previous != null)
                Previous.Next = Next;

            Previous = Next = null;
        }
        
        public void InsertBefore(Instruction i)
        {
            i.Remove();
            i.Previous = Previous;
            i.Next = this;
            if(Previous != null)
                Previous.Next = i;
            Previous = i;
        }

        public void InsertAfter(Instruction i)
        {
            i.Remove();
            i.Previous = this;
            i.Next = Next;
            if(Next != null)
                Next.Previous = i;
            Next = i;
        }
    }
}