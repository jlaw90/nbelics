﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class CheckCast: Instruction
    {
        public override byte Opcode
        {
            get { return 0xc0; }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }
        public ConstantClass CheckType { get; set; }

        public CheckCast(ConstantClass type)
        {
            CheckType = type;
        }
    }
}
