﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class ArrayLength: Instruction
    {
        public override byte Opcode
        {
            get { return 0xbe; }
        }
        public override ushort StackProduced
        {
            get { return 1; }
        }
        public override ushort StackConsumed
        {
            get { return 1; }
        }
    }
}
