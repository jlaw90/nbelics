﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class ArrayLoad : Instruction
    {
        public override byte Opcode
        {
            get
            {
                switch (ArrayType)
                {
                    case JavaType.Integer:
                        return 0x2e;
                    case JavaType.Long:
                        return 0x2f;
                    case JavaType.Float:
                        return 0x30;
                    case JavaType.Double:
                        return 0x31;
                    case JavaType.Reference:
                        return 0x32;
                    case JavaType.Boolean:
                    case JavaType.Byte:
                        return 0x33;
                    case JavaType.Character:
                        return 0x34;
                    case JavaType.Short:
                        return 0x35;
                }
                throw new InvalidOperationException();
            }
        }

        public override ushort StackProduced
        {
            get { return (ushort) ((ArrayType == JavaType.Long || ArrayType == JavaType.Double)? 2: 1); }
        }
        public override ushort StackConsumed
        {
            get { return 2; }
        }

        public JavaType ArrayType { get; set; }

        public ArrayLoad(JavaType aType)
        {
            ArrayType = aType;
        }
    }
}
