﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class LoadConstant: Instruction
    {
        public override byte Opcode
        {
            get
            {
                return (byte) (IsLdc2? 0x14: IsWide ? 0x13 : 0x12);
            }
        }
        public override uint Size
        {
            get
            {
                return (byte) ((IsWide || IsLdc2)? 2: 3);
            }
        }
        public bool IsWide
        {
            get { return Constant.Index > 255; }
        }
        public bool IsLdc2
        {
            get { return Constant.GetType() == typeof (ConstantLong) || Constant.GetType() == typeof (ConstantDouble); }
        }

        private ConstantEntry _constant;
        public ConstantEntry Constant
        {
            get { return _constant; }
            set
            {
                var t = value.GetType();
                if(t != typeof(ConstantInteger) && t != typeof(ConstantFloat) && t != typeof(ConstantClass) &&
                    t != typeof(ConstantLong) && t != typeof(ConstantDouble) && t != typeof(ConstantString) &&
                    t != typeof(ConstantMethodType) && t != typeof(ConstantMethodHandle))
                    throw new InvalidDataException("Constants that can be loaded: int, float, long, double, class, string, method type and method handle ONLY.");
                _constant = value;
            }
        }

        public LoadConstant(ConstantEntry e)
        {
            Constant = e;
        }
    }
}