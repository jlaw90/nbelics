﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class GetField : Instruction
    {
        public override byte Opcode
        {
            get { return 0xb4; }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 1;
            }
        }
        public override ushort StackProduced
        {
            get
            {
                // Todo: calculate whether field is double or long and modify accordingly
                throw new NotImplementedException();
            }
        }
        public ConstantFieldRef Field { get; set; }

        public GetField(ConstantFieldRef fr)
        {
            Field = fr;
        }
    }
}