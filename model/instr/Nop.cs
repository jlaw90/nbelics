﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Nop: Instruction
    {
        public override byte Opcode
        {
            get { return 0x0; }
        }
    }
}
