﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public interface IWideInstruction
    {
        bool IsWide { get; }
    }
}
