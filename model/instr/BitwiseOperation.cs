﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class BitwiseOperation : Instruction
    {
        public enum Operator
        {
            ShiftLeft,
            ShiftRight,
            UnsignedShiftRight,
            And,
            Or,
            ExclusiveOr
        }

        public override byte Opcode
        {
            get
            {
                switch (Operation)
                {
                    case Operator.ShiftLeft:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x78; // ishl
                            case JavaType.Long:
                                return 0x79; // lshl
                        }
                        break;
                    case Operator.ShiftRight:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x7a; // ishr
                            case JavaType.Long:
                                return 0x7b; // lshr
                        }
                        break;
                    case Operator.UnsignedShiftRight:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x7c; // iushr
                            case JavaType.Long:
                                return 0x7d; // lushr
                        }
                        break;
                    case Operator.And:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x7e; // iand
                            case JavaType.Long:
                                return 0x7f; // land
                        }
                        break;
                    case Operator.Or:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x80; // ior
                            case JavaType.Long:
                                return 0x81; // lor
                        }
                        break;
                    case Operator.ExclusiveOr:
                        switch (Type)
                        {
                            case JavaType.Integer:
                                return 0x82; // ixor
                            case JavaType.Long:
                                return 0x83; // lxor
                        }
                        break;
                }
                throw new InvalidDataException();
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return (ushort) ((Type == JavaType.Double || Type == JavaType.Long)? 2: 1);
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                switch (Type)
                {
                    case JavaType.Double:
                    case JavaType.Long:
                        switch (Operation)
                        {
                            case Operator.ShiftRight:
                            case Operator.ShiftLeft:
                            case Operator.UnsignedShiftRight:
                                return 3;
                            default:
                                return 4;
                        }
                    default:
                        return 2;
                }
            }
        }
        public Operator Operation { get; set; }
        private JavaType _type;
        public JavaType Type
        {
            get { return _type; }
            set
            {
                if (value != JavaType.Integer && value != JavaType.Long)
                    throw new InvalidDataException("Valid types for bitwise operations are int and long");
                _type = value;
            }
        }

        public BitwiseOperation(JavaType type, Operator o)
        {
            Operation = o;
            Type = type;
        }
    }
}
