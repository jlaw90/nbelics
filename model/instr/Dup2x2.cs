﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Dup2x2 : Instruction
    {
        public override byte Opcode
        {
            get { return 0x5d; }
        }
        public override ushort StackProduced
        {
            get
            {
                return 6;
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 4;
            }
        }
    }
}
