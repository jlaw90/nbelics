﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Convert : Instruction
    {
        public override byte Opcode
        {
            get
            {
                switch (From)
                {
                    case JavaType.Integer:
                        switch (To)
                        {
                            case JavaType.Byte:
                                return 0x91; // i2b
                            case JavaType.Character:
                                return 0x92; // i2c
                            case JavaType.Short:
                                return 0x93; // i2s
                            case JavaType.Long:
                                return 0x85; // i2l
                            case JavaType.Float:
                                return 0x86; // i2f
                            case JavaType.Double:
                                return 0x87; // i2d
                        }
                        break;
                    case JavaType.Long:
                        switch (To)
                        {
                            case JavaType.Integer:
                                return 0x88; // l2i
                            case JavaType.Float:
                                return 0x89; // l2f
                            case JavaType.Double:
                                return 0x8a; // l2d
                        }
                        break;
                    case JavaType.Float:
                        switch (To)
                        {
                            case JavaType.Integer:
                                return 0x8b; // f2i
                            case JavaType.Long:
                                return 0x8c; // f2l
                            case JavaType.Double:
                                return 0x8d; // f2d
                        }
                        break;
                    case JavaType.Double:
                        switch (To)
                        {
                            case JavaType.Integer:
                                return 0x8e; // d2i
                            case JavaType.Long:
                                return 0x8f; // d2l
                            case JavaType.Float:
                                return 0x90; // d2f
                        }
                        break;
                }
                throw new InvalidDataException();
            }
        }

        public override ushort StackProduced
        {
            get
            {
                return (ushort)((To == JavaType.Double || To == JavaType.Long) ? 2 : 1);
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return (ushort) ((From == JavaType.Double || From == JavaType.Long)? 2: 1);
            }
        }

        public JavaType From { get; private set; }
        public JavaType To { get; private set; }

        public Convert(JavaType from, JavaType to)
        {
            ChangeConversion(from, to);
        }

        public void ChangeConversion(JavaType from, JavaType to)
        {
            Verify(from, to);
            From = from;
            To = to;
        }

        private static void Verify(JavaType f, JavaType t)
        {
            switch (f)
            {
                case JavaType.Integer:
                    if (t != JavaType.Byte && t != JavaType.Character && t != JavaType.Short && t != JavaType.Long && t != JavaType.Float && t != JavaType.Double)
                        throw new InvalidDataException("int can only be converted to byte, char, short, long, float or double.");
                    break;
                case JavaType.Long:
                    if (t != JavaType.Integer && t != JavaType.Float && t != JavaType.Double)
                        throw new InvalidDataException("long can only be converted to int, float or double.");
                    break;
                case JavaType.Float:
                    if (t != JavaType.Integer && t != JavaType.Long && t != JavaType.Double)
                        throw new InvalidDataException("float can only be converted to int, long or double.");
                    break;
                case JavaType.Double:
                    if (t != JavaType.Integer && t != JavaType.Long && t != JavaType.Float)
                        throw new InvalidDataException("double can only be converted to int, long or float.");
                    break;
                default:
                    throw new InvalidDataException("Cannot convert from source type: must be int, float, long or double.");

            }
        }
    }
}