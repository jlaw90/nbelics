﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Store : Instruction, IWideInstruction
    {
        public override byte Opcode
        {
            get
            {
                switch (StoreType)
                {
                    case JavaType.Byte:
                    case JavaType.Boolean:
                    case JavaType.Character:
                    case JavaType.Short:
                    case JavaType.Integer:
                        switch (StoreIndex)
                        {
                            case 0:
                                return 0x3b;
                            case 1:
                                return 0x3c;
                            case 2:
                                return 0x3d;
                            case 3:
                                return 0x3e;
                            default:
                                return 0x36;
                        }
                    case JavaType.Long:
                        switch (StoreIndex)
                        {
                            case 0:
                                return 0x3f;
                            case 1:
                                return 0x40;
                            case 2:
                                return 0x41;
                            case 3:
                                return 0x42;
                            default:
                                return 0x37;
                        }
                    case JavaType.Float:
                        switch (StoreIndex)
                        {
                            case 0:
                                return 0x43;
                            case 1:
                                return 0x44;
                            case 2:
                                return 0x45;
                            case 3:
                                return 0x46;
                            default:
                                return 0x38;
                        }
                    case JavaType.Double:
                        switch (StoreIndex)
                        {
                            case 0:
                                return 0x47;
                            case 1:
                                return 0x48;
                            case 2:
                                return 0x49;
                            case 3:
                                return 0x4a;
                            default:
                                return 0x39;
                        }
                    case JavaType.Reference:
                        switch (StoreIndex)
                        {
                            case 0:
                                return 0x4b;
                            case 1:
                                return 0x4c;
                            case 2:
                                return 0x4d;
                            case 3:
                                return 0x4e;
                            default:
                                return 0x3a;
                        }
                }
                throw new InvalidEnumArgumentException("StoreType is not set to a value");
            }
        }

        public override uint Size
        {
            get
            {
                return (byte)(1 + (StoreIndex > 3 ? 1 : 0) + (IsWide ? 1 : 0));
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return (ushort) (StoreType == JavaType.Double || StoreType == JavaType.Long? 2: 1);
            }
        }
        public bool IsWide { get { return StoreIndex > 255; } }
        public JavaType StoreType { get; set; }
        public ushort StoreIndex { get; set; }

        public Store(JavaType t, ushort idx)
        {
            StoreType = t;
            StoreIndex = idx;
        }
    }
}
