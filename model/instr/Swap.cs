﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Swap: Instruction
    {
        public override byte Opcode
        {
            get { return 0x5f; }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 2;
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return 2;
            }
        }
    }
}