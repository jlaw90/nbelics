﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class New: Instruction
    {
        public override byte Opcode
        {
            get { return 0xbb; }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return 1;
            }
        }

        public ConstantClass Class { get; set; }

        public New(ConstantClass c)
        {
            Class = c;
        }
    }
}