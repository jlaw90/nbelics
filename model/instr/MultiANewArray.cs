﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class MultiANewArray: Instruction
    {
        public override byte Opcode
        {
            get { return 0xc5; }
        }
        public override uint Size
        {
            get
            {
                return 4;
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return (ushort) Dimensions;
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return 1;
            }
        }

        public ConstantClass Type { get; set; }
        public byte Dimensions { get; set; }

        public MultiANewArray(ConstantClass t, byte dimensions)
        {
            Type = t;
            Dimensions = dimensions;
        }
    }
}