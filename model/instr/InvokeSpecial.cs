﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class InvokeSpecial: Instruction 
    {
        public override byte Opcode
        {
            get { return 0xb7; }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }
        // Todo: Calculate stack consumed and produced
        public override ushort StackConsumed
        {
            get
            {
                return base.StackConsumed;
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return base.StackProduced;
            }
        }
        public ConstantMethodRef Method { get; set; }

        public InvokeSpecial(ConstantMethodRef cmr)
        {
            Method = cmr;
        }
    }
}