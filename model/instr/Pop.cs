﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Pop: Instruction
    {
        public override byte Opcode
        {
            get { return 0x57; }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 1;
            }
        }
    }
}