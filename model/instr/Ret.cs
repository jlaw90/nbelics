﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Ret: Instruction, IWideInstruction
    {
        public override byte Opcode
        {
            get { return 0xa9; }
        }
        public override uint Size
        {
            get
            {
                return (byte) (IsWide? 3: 2);
            }
        }
        public bool IsWide
        {
            get { return ReturnIndex > 255; }
        }
        public ushort ReturnIndex { get; set; }

        public Ret(ushort retIdx)
        {
            ReturnIndex = retIdx;
        }
    }
}
