﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class IInc: Instruction, IWideInstruction
    {
        public override byte Opcode
        {
            get { return 0x84; }
        }
        public override uint Size { get { return (byte) (IsWide ? 5 : 3); } }
        public bool IsWide
        {
            get { return LocalVariableIndex > 255 || (Increment > SByte.MaxValue || Increment < SByte.MinValue); }
        }
        public ushort LocalVariableIndex { get; set; }
        public short Increment { get; set; }

        public IInc(ushort localVar, short inc)
        {
            LocalVariableIndex = localVar;
            Increment = inc;
        }
    }
}