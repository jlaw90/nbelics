﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Jsr: BranchInstruction
    {
        public override byte Opcode
        {
            get { return (byte) (IsWide ? 0xc9 : 0xa8); }
        }
        public override uint Size
        {
            get
            {
                return (byte) (IsWide? 5: 3);
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return 1;
            }
        }
        public bool IsWide { get
        {
            var dif = Branch.Location - Location;
            return dif < short.MinValue || dif > short.MaxValue;
        } }

        public Jsr(Instruction _b)
        {
            Branch = _b;
        }

        internal Jsr(int _branch)
        {
            this._branch = _branch;
        }
    }
}