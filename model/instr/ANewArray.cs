﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class ANewArray: Instruction
    {
        public override byte Opcode
        {
            get { return 0xbd; }
        }
        public override uint Size
        {
            get { return (byte) 3; }
        }
        public override ushort StackProduced
        {
            get { return 1; }
        }
        public override ushort StackConsumed
        {
            get { return 1; }
        }

        public ConstantClass ArrayType { get; set; }

        public ANewArray(ConstantClass type)
        {
            ArrayType = type;
        }

        public override string ToString()
        {
            return "anewarray " + ArrayType;
        }
    }
}
