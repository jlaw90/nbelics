﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Compare : Instruction
    {


        /// <summary>
        /// When the JVM encounters NaN (e.g. if(1.0f >= Float.NaN), it is designed to always be false
        /// So in the case of &gt; operator, GreaterThan comparison should be used, and when &lt;
        /// is used, the LessThan comparison should be used. (cmpg pushes 1 on NaN, cpml pushes -1 on NaN)
        /// </summary>
        public enum ComparisonType
        {
            GreaterThan,
            LessThan
        }

        public override byte Opcode
        {
            get
            {
                switch (Type)
                {
                    case JavaType.Long:
                        return 0x94; // lcmp
                    case JavaType.Float:
                        switch (FloatingPointComparison)
                        {
                            case ComparisonType.LessThan:
                                return 0x95; // fcmpl
                            case ComparisonType.GreaterThan:
                                return 0x96; // fcmpg
                        }
                        break;
                    case JavaType.Double:
                        switch (FloatingPointComparison)
                        {
                            case ComparisonType.LessThan:
                                return 0x97; // dcmpl
                            case ComparisonType.GreaterThan:
                                return 0x98; // dcmpg
                        }
                        break;
                }
                throw new InvalidDataException();
            }
        }
        public override ushort StackProduced { get { return 1; } }
        public override ushort StackConsumed { get { return (ushort) ((Type == JavaType.Double || Type == JavaType.Long)? 4: 2); } }
        private JavaType _type;
        public JavaType Type
        {
            get { return _type; }
            set
            {
                if (value != JavaType.Double && value != JavaType.Float && value != JavaType.Long)
                    throw new DataException("Type for ComparisonInstruction must be either float, double or long");
                if ((value == JavaType.Double || value == JavaType.Float) && !FloatingPointComparison.HasValue)
                    throw new DataException("When specifying a floating point type, make sure you specify the FloatingPointComparison first");
                _type = value;
            }
        }

        private ComparisonType? _floatingPointComparison;

        /// <summary>
        /// If Type is Float or Double, this indicates how they will deal with NaN
        /// </summary>
        public ComparisonType? FloatingPointComparison
        {
            get { return _floatingPointComparison; }
            set
            {
                if ((Type == JavaType.Double || Type == JavaType.Float) && !value.HasValue)
                    throw new InvalidDataException("When comparing a floating point type, the FloatingPointComparison MUST be set");
                _floatingPointComparison = value;
            }
        }

        public Compare(JavaType fpType, ComparisonType? comparison)
        {
            if ((fpType == JavaType.Double || fpType == JavaType.Float) && !comparison.HasValue)
                throw new InvalidDataException("If Type is double or float, you must specify a floating point comparison type");
            FloatingPointComparison = comparison;
            Type = fpType;
        }
    }
}