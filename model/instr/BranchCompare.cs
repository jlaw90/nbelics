﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class BranchCompare : BranchInstruction
    {
        public enum Operator
        {
            Equal,
            NotEqual,
            GreaterThan,
            LessThan,
            GreaterThanOrEqual,
            LessThanOrEqual
        }

        public override byte Opcode
        {
            get
            {
                switch (Type)
                {
                    case JavaType.Integer:
                        switch (Comparison)
                        {
                            case Operator.Equal:
                                return 0x9f; // if_icmpeq
                            case Operator.NotEqual:
                                return 0xa0; // if_icmpne
                            case Operator.LessThan:
                                return 0xa1; // if_icmplt
                            case Operator.GreaterThanOrEqual:
                                return 0xa2; // if_icmpge
                            case Operator.GreaterThan:
                                return 0xa3; // if_icmpgt
                            case Operator.LessThanOrEqual:
                                return 0xa4;
                        }
                        break;
                    case JavaType.Reference:
                        switch (Comparison)
                        {
                            case Operator.Equal:
                                return 0xa5; // if_acmpeq
                            case Operator.NotEqual:
                                return 0xa6; // if_acmpne
                        }
                        break;
                }
                throw new InvalidDataException();
            }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 2;
            }
        }

        public JavaType Type { get; private set; }
        public Operator Comparison { get; private set; }

        public BranchCompare(JavaType t, Operator o, Instruction branch)
        {
            ChangeComparison(t, o);
            Branch = branch;
        }

        internal BranchCompare(JavaType t, Operator o, short _branch)
        {
            Verify(t, o);
            Type = t;
            Comparison = o;
            this._branch = _branch;
        }

        public void ChangeComparison(JavaType t, Operator o)
        {
            Verify(t, o);
            Type = t;
            Comparison = o;
        }

        private static void Verify(JavaType t, Operator c)
        {
            if (t == JavaType.Reference && c != Operator.Equal && c != Operator.NotEqual)
                throw new InvalidDataException("If the type being compared is a reference, you can only compare if they are equal or not equal.");
            if (t != JavaType.Reference && t != JavaType.Integer)
                throw new InvalidDataException("The only types that can be compared by the branch compare are int and reference.");
        }
    }
}
