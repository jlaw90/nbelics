﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class InstructionList: IEnumerable<Instruction>
    {
        private Instruction _root = new Sentinel();

        public Instruction Root { get { return _root; } }

        public Instruction Offset(uint off)
        {
            // Find instruction with this offset
            var i = _root;
            uint ioff = 0;
            while(i.Next != null)
            {
                i = i.Next;
                if(ioff == off)
                    return i;
                ioff += i.Size;
            }
            return null;
        }

        public Instruction Index(uint off)
        {
            var i = _root;
            var idx = 0;
            while(i.Next != null)
            {
                i = i.Next;
                if (idx == off)
                    return i;
                idx++;
            }
            return null;
        }

        public List<Instruction> List
        {
            get
        {
            var l = new List<Instruction>();
            var i = _root;
            if (i.Next == null)
                return l;
            do
            {
                i = i.Next;
                l.Add(i);
            } while (i.Next != null);
            return l;
        }}

        public IEnumerator<Instruction> GetEnumerator()
        {
            return new InstructionEnumerator(_root);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private class Sentinel: Instruction
        {
            public override byte Opcode
            {
                get { return (byte) 0x0; }
            }
            public override uint Size
            {
                get
                {
                    return 0;
                }
            }

            public override ushort StackConsumed
            {
                get
                {
                    return 0;
                }
            }

            public override ushort StackProduced
            {
                get
                {
                    return 0;
                }
            }
        }

        private class InstructionEnumerator: IEnumerator<Instruction>
        {
            private Instruction _root;
            private Instruction _current;

            internal InstructionEnumerator(Instruction root)
            {
                this._root = root;
            }

            public void Dispose()
            {
                _root = null;
                _current = null;
            }

            public bool MoveNext()
            {
                if(_current == null)
                {
                    _current = _root.Next;
                    return true;
                }
                if(_current.Next == null)
                {
                    _current = null;
                    return false;
                }
                _current = _current.Next;
                return true;
            }

            public void Reset()
            {
                _current = null;
            }

            public Instruction Current { get
            {
                if(_current == null)
                    throw new InvalidOperationException();
                return _current;
            }}

            object IEnumerator.Current { get { return Current; } }
        }
    }
}