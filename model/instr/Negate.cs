﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Negate : Instruction
    {
        public override byte Opcode
        {
            get
            {
                switch (Type)
                {
                    case JavaType.Integer:
                        return 0x74; // ineg
                    case JavaType.Long:
                        return 0x75; // lneg
                    case JavaType.Float:
                        return 0x76; // fneg
                    case JavaType.Double:
                        return 0x77; // dneg
                }
                throw new InvalidDataException();
            }
        }
        public override ushort StackProduced
        {
            get
            {
                return (ushort)((Type == JavaType.Double || Type == JavaType.Long) ? 2 : 1);
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return (ushort) ((Type == JavaType.Double || Type == JavaType.Long)? 2: 1);
            }
        }
        private JavaType _type;
        public JavaType Type
        {
            get { return _type; }
            set
            {
                if (value != JavaType.Integer && value != JavaType.Long && value != JavaType.Float && value != JavaType.Double)
                    throw new InvalidDataException("NegateInstruction can only negates types of int, long, float and double");
                _type = value;
            }
        }

        public Negate(JavaType t)
        {
            Type = t;
        }
    }
}
