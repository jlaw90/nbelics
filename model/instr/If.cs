﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class If : BranchInstruction
    {
        public enum Operator
        {
            Equal,
            NotEqual,
            GreaterThan,
            LessThan,
            GreaterThanOrEqual,
            LessThanOrEqual
        }

        public override byte Opcode
        {
            get
            {
                switch (Comparison)
                {
                    case Operator.Equal:
                        return 0x99;
                    case Operator.NotEqual:
                        return 0x9a;
                    case Operator.LessThan:
                        return 0x9b;
                    case Operator.GreaterThanOrEqual:
                        return 0x9c;
                    case Operator.GreaterThan:
                        return 0x9d;
                    case Operator.LessThanOrEqual:
                        return 0x9e;
                }
                throw new InvalidDataException();
            }
        }
        public override uint Size
        {
            get
            {
                return 3;
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 1;
            }
        }

        public Operator Comparison { get; set; }

        public If(Operator comp, Instruction branch)
        {
            Comparison = comp;
            Branch = branch;
        }

        internal If(Operator op, short _branch)
        {
            this._branch = _branch;
            Comparison = op;
        }
    }
}