﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.instr
{
    public class Dupx1: Instruction 
    {
        public override byte Opcode
        {
            get { return 0x5a; }
        }
        public override ushort StackProduced
        {
            get
            {
                return 3;
            }
        }
        public override ushort StackConsumed
        {
            get
            {
                return 2;
            }
        }
    }
}
