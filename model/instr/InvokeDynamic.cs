﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.pool;

namespace NBELICS.model.instr
{
    public class InvokeDynamic: Instruction
    {
        public override byte Opcode
        {
            get { return 0xba; }
        }
        public override uint Size
        {
            get
            {
                return 5;
            }
        }
        // Todo: calculate stack consumed... (method args.)
        public override ushort StackConsumed
        {
            get
            {
                return base.StackConsumed;
            }
        }
        // Todo: calculate stack produced
        public override ushort StackProduced
        {
            get
            {
                return base.StackProduced;
            }
        }

        public ConstantMethodHandle Method { get; set; }

        public InvokeDynamic(ConstantMethodHandle h)
        {
            Method = h;
        }
    }
}