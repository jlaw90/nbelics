﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.attr
{
    public class UnknownAttribute: Attribute
    {
        public byte[] Data { get; set; }
    }
}
