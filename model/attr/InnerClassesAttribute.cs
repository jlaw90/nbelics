﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class InnerClassesAttribute: Attribute
    {
        public List<InnerClass> InnerClasses { get; private set; } 

        internal InnerClassesAttribute()
        {
        }

        internal new static InnerClassesAttribute Read(ConstantPool cp, DataInputStreamReader disr)
        {
            var len = disr.ReadUInt();
            var count = disr.ReadUShort();
            var l = new List<InnerClass>();
            for(var i = 0; i < count; i++)
            {
                l.Add(new InnerClass
                          {
                              InnerClassInfo = (ConstantClass)cp[disr.ReadUShort()],
                              OuterClassInfo = (ConstantClass)cp[disr.ReadUShort()],
                              InnerNameReference = (ConstantUtf8) cp[disr.ReadUShort()],
                              AccessFlags = (AccessFlag) disr.ReadUShort()
                          });
            }

            return new InnerClassesAttribute {InnerClasses = l};
        }
    }

    public class InnerClass
    {
        public ConstantClass InnerClassInfo { get; set; }
        public ConstantClass OuterClassInfo { get; set; }
        public ConstantUtf8 InnerNameReference { get; set; }
        public string InnerName { get { return InnerNameReference.Constant; } set { InnerNameReference.Constant = value; } }
        public AccessFlag AccessFlags { get; set; }

        internal InnerClass()
        {
        }
    }
}
