﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class SourceDebugExtensionAttribute: Attribute
    {
        public byte[] DebugExtension { get; set; }

        internal SourceDebugExtensionAttribute()
        {
        }

        internal new static SourceDebugExtensionAttribute Read(ConstantPool cp, DataInputStreamReader disr)
        {
            var len = disr.ReadUInt();
            var data = new byte[disr.ReadUInt()];
            disr.ReadFully(data, 0, data.Length);
            return new SourceDebugExtensionAttribute {DebugExtension = data};
        }
    }
}
