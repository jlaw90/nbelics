﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.instr;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class LocalVariableTypeTableAttribute : Attribute
    {
        public List<LocalVariableTypeTableEntry> LocalVariableTable { get; private set; }

        internal LocalVariableTypeTableAttribute()
        {
        }

        internal static LocalVariableTypeTableAttribute Read(ConstantPool cp, CodeAttribute ca, DataInputStreamReader disr)
        {
            var len = disr.ReadInt();
            var count = disr.ReadUShort();
            var l = new List<LocalVariableTypeTableEntry>();
            for (var i = 0; i < count; i++)
            {
                uint start = disr.ReadUShort();
                var end = start + disr.ReadUShort();
                l.Add(new LocalVariableTypeTableEntry
                {
                    Start = ca.Code.Offset(start),
                    End = ca.Code.Offset(end),
                    NameReference = (ConstantUtf8)cp[disr.ReadUShort()],
                    SignatureReference = (ConstantUtf8)cp[disr.ReadUShort()],
                    Index = disr.ReadUShort()
                });
            }

            return new LocalVariableTypeTableAttribute { LocalVariableTable = l };
        }
    }

    public class LocalVariableTypeTableEntry
    {
        public Instruction Start { get; set; }
        public Instruction End { get; set; }
        public ConstantUtf8 NameReference { get; set; }
        public string Name { get { return NameReference.Constant; } set { NameReference.Constant = value; } }
        public ConstantUtf8 SignatureReference { get; set; }
        public string Signature { get { return SignatureReference.Constant; } set { SignatureReference.Constant = value; } }
        public ushort Index { get; set; }

        internal LocalVariableTypeTableEntry(){}
    }
}