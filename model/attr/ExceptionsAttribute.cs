﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class ExceptionsAttribute: Attribute
    {
        public List<ConstantClass> Exceptions { get; private set; } 

        internal ExceptionsAttribute()
        {
            
        }

        internal new static ExceptionsAttribute Read(ConstantPool cp, DataInputStreamReader disr)
        {
            var len = disr.ReadUInt();
            var num = disr.ReadUShort();
            var l = new List<ConstantClass>();
            for (var i = 0; i < num; i++)
                l.Add((ConstantClass) cp[disr.ReadUShort()]);

            return new ExceptionsAttribute {Exceptions = l};
        }
    }
}
