﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class ConstantValueAttribute: Attribute
    {
        public ConstantValueEntry Constant { get; set; }

        public override string ToString()
        {
            return Constant.ToString();
        }

        internal ConstantValueAttribute(ConstantValueEntry icv)
        {
            Constant = icv;
        }

        internal new static ConstantValueAttribute Read(ConstantPool cp, DataInputStreamReader disr)
        {
            var len = disr.ReadUInt();
            if(len != 2)
                throw new DataException("Invalid ConstantValueAttribute definition");
            var idx = ((ConstantValueEntry)cp[disr.ReadUShort()]);
            return new ConstantValueAttribute(idx);
        }
    }
}
