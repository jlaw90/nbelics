﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.instr;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class LineNumberTableAttribute: Attribute
    {
        public Dictionary<Instruction, ushort> LineNumberTable { get; private set; } 

        internal LineNumberTableAttribute()
        {
        }

        internal new static LineNumberTableAttribute Read(ConstantPool cp, CodeAttribute ca, DataInputStreamReader disr)
        {
            var len = disr.ReadUInt();
            var count = disr.ReadUShort();
            var dict = new Dictionary<Instruction, ushort>();
            for (var i = 0; i < count; i++)
                dict[ca.Code.Offset(disr.ReadUShort())] = disr.ReadUShort();

            return new LineNumberTableAttribute {LineNumberTable = dict};
        }
    }
}