﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public abstract class Attribute
    {
        internal ConstantUtf8 _nameRef;
        public ConstantUtf8 NameReference { get { return _nameRef; } set { _nameRef = value; } }
        public string Name { get { return _nameRef.Constant; } set { _nameRef.Constant = value; } }

        // Todo: implement stackmaptable attribute (fuck, a lot of code...)
        internal static Attribute Read(ConstantPool cp, DataInputStreamReader disr)
        {
            var anr = (ConstantUtf8)cp[disr.ReadUShort()];
            Attribute atr = null;
            switch (anr.Constant)
            {
                case "ConstantValue":
                    atr = ConstantValueAttribute.Read(cp, disr);
                    break;

                case "Synthetic":
                    atr = SyntheticAttribute.Read(cp, disr);
                    break;

                case "Signature":
                    atr = SignatureAttribute.Read(cp, disr);
                    break;

                case "SourceFile":
                    atr = SourceFileAttribute.Read(cp, disr);
                    break;

                case "SourceDebugExtensions":
                    atr = SourceDebugExtensionAttribute.Read(cp, disr);
                    break;

                case "Deprecated":
                    atr = DeprecatedAttribute.Read(cp, disr);
                    break;

                case "Code":
                    atr = CodeAttribute.Read(cp, disr);
                    break;

                case "Exceptions":
                    atr = ExceptionsAttribute.Read(cp, disr);
                    break;

                case "InnerClasses":
                    atr = InnerClassesAttribute.Read(cp, disr);
                    break;

                default:
                    Console.Error.WriteLine("Unhandled attribute: " + anr);
                    var len = disr.ReadUInt();
                    var data = new byte[len];
                    disr.ReadFully(data, 0, (int)len);
                    atr = new UnknownAttribute { Data = data };
                    break;
            }
            atr._nameRef = anr;
            return atr;
        }
    }
}