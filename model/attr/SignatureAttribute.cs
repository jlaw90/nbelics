﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class SignatureAttribute: Attribute
    {
        public ConstantUtf8 SignatureReference { get; set; }
        public string Signature { get { return SignatureReference.Constant; } set { SignatureReference.Constant = value; } }

        internal SignatureAttribute()
        {
            
        }

        internal new static SignatureAttribute Read(ConstantPool cp, DataInputStreamReader disr)
        {
            var len = disr.ReadUInt();
            if(len != 2)
                throw new DataException();
            return new SignatureAttribute {SignatureReference = (ConstantUtf8) cp[disr.ReadUShort()]};
        }
    }
}
