﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class SyntheticAttribute: Attribute
    {
        internal SyntheticAttribute()
        {
        }

        internal new static SyntheticAttribute Read(ConstantPool cp, DataInputStreamReader disr)
        {
            var len = disr.ReadUInt();
            if(len != 0)
                throw new DataException("Invalid SyntheticAttribute");
            return new SyntheticAttribute();
        }
    }
}