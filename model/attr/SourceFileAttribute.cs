﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class SourceFileAttribute : Attribute
    {
        public ConstantUtf8 SourceFileReference { get; set; }
        public string SourceFile { get { return SourceFileReference.Constant; } set { SourceFileReference.Constant = value; } }

        internal SourceFileAttribute()
        {
        }

        internal new static SourceFileAttribute Read(ConstantPool cp, DataInputStreamReader disr)
        {
            var len = disr.ReadUInt();
            if (len != 2)
                throw new DataException();
            return new SourceFileAttribute {SourceFileReference = (ConstantUtf8) cp[disr.ReadUShort()]};
        }
}
}
