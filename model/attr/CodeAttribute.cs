﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.instr;
using NBELICS.model.pool;
using Convert = NBELICS.model.instr.Convert;
using Switch = NBELICS.model.instr.Switch;

namespace NBELICS.model.attr
{
    public class CodeAttribute : Attribute
    {
        public ushort MaxStack { get; set; }
        public ushort MaxLocals { get; set; }

        // Todo: process individual instructions
        public InstructionList Code { get; set; }
        public List<ExceptionTableEntry> ExceptionTable { get; private set; }
        public List<Attribute> Attributes { get; private set; }


        public ushort CalculatedMaxStack
        {
            get
            {
                var stack = (ushort) (1 + Code.Sum(i => i.StackProduced - i.StackConsumed));
                return stack;
            }
        }

        internal CodeAttribute()
        {

        }

        internal new static CodeAttribute Read(ConstantPool cp, DataInputStreamReader disr)
        {
            var len = disr.ReadUInt();
            var init = disr.Position;

            var maxStack = disr.ReadUShort();
            var maxLocals = disr.ReadUShort();
            var codeLen = disr.ReadUInt();
            var pos = disr.Position;
            var _i = new Dictionary<uint, Instruction>();
            #region Reading instructions
            var isWide = false;
            while (disr.Position < pos + codeLen)
            {
                var pc = (uint)(disr.Position - pos);
                var op = disr.ReadUByte();
                switch (op)
                {
                    #region Constants
                    case 0x0:
                        _i[pc] = new Nop();
                        break;
                    case 0x1:
                        _i[pc] = new PushConstant<object>(null); // aconst_null
                        break;
                    case 0x2:
                        _i[pc] = new PushConstant<int>(-1); // iconst_m1
                        break;
                    case 0x3:
                        _i[pc] = new PushConstant<int>(0); // iconst_0
                        break;
                    case 0x4:
                        _i[pc] = new PushConstant<int>(1); // iconst_1
                        break;
                    case 0x5:
                        _i[pc] = new PushConstant<int>(2); // iconst_2
                        break;
                    case 0x6:
                        _i[pc] = new PushConstant<int>(3); // iconst_3
                        break;
                    case 0x7:
                        _i[pc] = new PushConstant<int>(4); // iconst_4
                        break;
                    case 0x8:
                        _i[pc] = new PushConstant<int>(5); // iconst_5
                        break;
                    case 0x9:
                        _i[pc] = new PushConstant<long>(0); // lconst_0
                        break;
                    case 0xa:
                        _i[pc] = new PushConstant<long>(1); // lconst_1
                        break;
                    case 0xb:
                        _i[pc] = new PushConstant<float>(0); // fconst_0
                        break;
                    case 0xc:
                        _i[pc] = new PushConstant<float>(1); // fconst_1
                        break;
                    case 0xd:
                        _i[pc] = new PushConstant<float>(2); // fconst_2
                        break;
                    case 0xe:
                        _i[pc] = new PushConstant<double>(0); // dconst_0
                        break;
                    case 0xf:
                        _i[pc] = new PushConstant<double>(1); // dconst_1
                        break;
                    case 0x10:
                        _i[pc] = new Push(disr.ReadByte()); // bipush
                        break;
                    case 0x11:
                        _i[pc] = new Push(disr.ReadShort()); // sipush
                        break;
                    case 0x12:
                        _i[pc] = new LoadConstant(cp[disr.ReadUByte()]); // ldc
                        break;
                    case 0x13:
                        _i[pc] = new LoadConstant(cp[disr.ReadUShort()]); // ldc_w
                        break;
                    case 0x14:
                        _i[pc] = new LoadConstant(cp[disr.ReadUShort()]); // ldc2_w
                        break;

                    #endregion

                    #region Loads
                    case 0x15:
                        _i[pc] = new Load(JavaType.Integer, isWide ? disr.ReadUShort() : disr.ReadUByte()); // iload
                        break;
                    case 0x16:
                        _i[pc] = new Load(JavaType.Long, isWide ? disr.ReadUShort() : disr.ReadUByte()); // lload
                        break;
                    case 0x17:
                        _i[pc] = new Load(JavaType.Float, isWide ? disr.ReadUShort() : disr.ReadUByte()); // fload
                        break;
                    case 0x18:
                        _i[pc] = new Load(JavaType.Double, isWide ? disr.ReadUShort() : disr.ReadUByte()); // dload
                        break;
                    case 0x19:
                        _i[pc] = new Load(JavaType.Reference, isWide ? disr.ReadUShort() : disr.ReadUByte());
                        break;
                    case 0x1a:
                        _i[pc] = new Load(JavaType.Integer, 0);
                        break;
                    case 0x1b:
                        _i[pc] = new Load(JavaType.Integer, 1);
                        break;
                    case 0x1c:
                        _i[pc] = new Load(JavaType.Integer, 2);
                        break;
                    case 0x1d:
                        _i[pc] = new Load(JavaType.Integer, 3);
                        break;
                    case 0x1e:
                        _i[pc] = new Load(JavaType.Long, 0);
                        break;
                    case 0x1f:
                        _i[pc] = new Load(JavaType.Long, 1);
                        break;
                    case 0x20:
                        _i[pc] = new Load(JavaType.Long, 2);
                        break;
                    case 0x21:
                        _i[pc] = new Load(JavaType.Long, 3);
                        break;
                    case 0x22:
                        _i[pc] = new Load(JavaType.Float, 0);
                        break;
                    case 0x23:
                        _i[pc] = new Load(JavaType.Float, 1);
                        break;
                    case 0x24:
                        _i[pc] = new Load(JavaType.Float, 2);
                        break;
                    case 0x25:
                        _i[pc] = new Load(JavaType.Float, 3);
                        break;
                    case 0x26:
                        _i[pc] = new Load(JavaType.Double, 0);
                        break;
                    case 0x27:
                        _i[pc] = new Load(JavaType.Double, 1);
                        break;
                    case 0x28:
                        _i[pc] = new Load(JavaType.Double, 2);
                        break;
                    case 0x29:
                        _i[pc] = new Load(JavaType.Double, 3);
                        break;
                    case 0x2a:
                        _i[pc] = new Load(JavaType.Reference, 0);
                        break;
                    case 0x2b:
                        _i[pc] = new Load(JavaType.Reference, 1);
                        break;
                    case 0x2c:
                        _i[pc] = new Load(JavaType.Reference, 2);
                        break;
                    case 0x2d:
                        _i[pc] = new Load(JavaType.Reference, 3);
                        break;
                    case 0x2e:
                        _i[pc] = new ArrayLoad(JavaType.Integer);
                        break;
                    case 0x2f:
                        _i[pc] = new ArrayLoad(JavaType.Long);
                        break;
                    case 0x30:
                        _i[pc] = new ArrayLoad(JavaType.Float);
                        break;
                    case 0x31:
                        _i[pc] = new ArrayLoad(JavaType.Double);
                        break;
                    case 0x32:
                        _i[pc] = new ArrayLoad(JavaType.Reference);
                        break;
                    case 0x33:
                        _i[pc] = new ArrayLoad(JavaType.Byte);
                        break;
                    case 0x34:
                        _i[pc] = new ArrayLoad(JavaType.Character);
                        break;
                    case 0x35:
                        _i[pc] = new ArrayLoad(JavaType.Short);
                        break;


                    #endregion

                    #region Stores
                    case 0x36:
                        _i[pc] = new Store(JavaType.Integer, isWide ? disr.ReadUShort() : disr.ReadUByte()); // iload
                        break;
                    case 0x37:
                        _i[pc] = new Store(JavaType.Long, isWide ? disr.ReadUShort() : disr.ReadUByte()); // lload
                        break;
                    case 0x38:
                        _i[pc] = new Store(JavaType.Float, isWide ? disr.ReadUShort() : disr.ReadUByte()); // fload
                        break;
                    case 0x39:
                        _i[pc] = new Store(JavaType.Double, isWide ? disr.ReadUShort() : disr.ReadUByte()); // dload
                        break;
                    case 0x3a:
                        _i[pc] = new Store(JavaType.Reference, isWide ? disr.ReadUShort() : disr.ReadUByte());
                        break;
                    case 0x3b:
                        _i[pc] = new Store(JavaType.Integer, 0);
                        break;
                    case 0x3c:
                        _i[pc] = new Store(JavaType.Integer, 1);
                        break;
                    case 0x3d:
                        _i[pc] = new Store(JavaType.Integer, 2);
                        break;
                    case 0x3e:
                        _i[pc] = new Store(JavaType.Integer, 3);
                        break;
                    case 0x3f:
                        _i[pc] = new Store(JavaType.Long, 0);
                        break;
                    case 0x40:
                        _i[pc] = new Store(JavaType.Long, 1);
                        break;
                    case 0x41:
                        _i[pc] = new Store(JavaType.Long, 2);
                        break;
                    case 0x42:
                        _i[pc] = new Store(JavaType.Long, 3);
                        break;
                    case 0x43:
                        _i[pc] = new Store(JavaType.Float, 0);
                        break;
                    case 0x44:
                        _i[pc] = new Store(JavaType.Float, 1);
                        break;
                    case 0x45:
                        _i[pc] = new Store(JavaType.Float, 2);
                        break;
                    case 0x46:
                        _i[pc] = new Store(JavaType.Float, 3);
                        break;
                    case 0x47:
                        _i[pc] = new Store(JavaType.Double, 0);
                        break;
                    case 0x48:
                        _i[pc] = new Store(JavaType.Double, 1);
                        break;
                    case 0x49:
                        _i[pc] = new Store(JavaType.Double, 2);
                        break;
                    case 0x4a:
                        _i[pc] = new Store(JavaType.Double, 3);
                        break;
                    case 0x4b:
                        _i[pc] = new Store(JavaType.Reference, 0);
                        break;
                    case 0x4c:
                        _i[pc] = new Store(JavaType.Reference, 1);
                        break;
                    case 0x4d:
                        _i[pc] = new Store(JavaType.Reference, 2);
                        break;
                    case 0x4e:
                        _i[pc] = new Store(JavaType.Reference, 3);
                        break;
                    case 0x4f:
                        _i[pc] = new ArrayStore(JavaType.Integer);
                        break;
                    case 0x50:
                        _i[pc] = new ArrayStore(JavaType.Long);
                        break;
                    case 0x51:
                        _i[pc] = new ArrayStore(JavaType.Float);
                        break;
                    case 0x52:
                        _i[pc] = new ArrayStore(JavaType.Double);
                        break;
                    case 0x53:
                        _i[pc] = new ArrayStore(JavaType.Reference);
                        break;
                    case 0x54:
                        _i[pc] = new ArrayStore(JavaType.Byte);
                        break;
                    case 0x55:
                        _i[pc] = new ArrayStore(JavaType.Character);
                        break;
                    case 0x56:
                        _i[pc] = new ArrayStore(JavaType.Short);
                        break;
                    #endregion

                    #region Stack
                    case 0x57:
                        _i[pc] = new Pop();
                        break;
                    case 0x58:
                        _i[pc] = new Pop2();
                        break;
                    case 0x59:
                        _i[pc] = new Dup();
                        break;
                    case 0x5a:
                        _i[pc] = new Dupx1();
                        break;
                    case 0x5b:
                        _i[pc] = new Dupx2();
                        break;
                    case 0x5c:
                        _i[pc] = new Dup2();
                        break;
                    case 0x5d:
                        _i[pc] = new Dup2x1();
                        break;
                    case 0x5e:
                        _i[pc] = new Dup2x2();
                        break;
                    case 0x5f:
                        _i[pc] = new Swap();
                        break;

                    #endregion

                    #region Math
                    case 0x60:
                        _i[pc] = new MathOperation(JavaType.Integer, MathOperation.Operator.Add);
                        break;
                    case 0x61:
                        _i[pc] = new MathOperation(JavaType.Long, MathOperation.Operator.Add);
                        break;
                    case 0x62:
                        _i[pc] = new MathOperation(JavaType.Float, MathOperation.Operator.Add);
                        break;
                    case 0x63:
                        _i[pc] = new MathOperation(JavaType.Double, MathOperation.Operator.Add);
                        break;
                    case 0x64:
                        _i[pc] = new MathOperation(JavaType.Integer, MathOperation.Operator.Subtract);
                        break;
                    case 0x65:
                        _i[pc] = new MathOperation(JavaType.Long, MathOperation.Operator.Subtract);
                        break;
                    case 0x66:
                        _i[pc] = new MathOperation(JavaType.Float, MathOperation.Operator.Subtract);
                        break;
                    case 0x67:
                        _i[pc] = new MathOperation(JavaType.Double, MathOperation.Operator.Subtract);
                        break;
                    case 0x68:
                        _i[pc] = new MathOperation(JavaType.Integer, MathOperation.Operator.Multiply);
                        break;
                    case 0x69:
                        _i[pc] = new MathOperation(JavaType.Long, MathOperation.Operator.Multiply);
                        break;
                    case 0x6a:
                        _i[pc] = new MathOperation(JavaType.Float, MathOperation.Operator.Multiply);
                        break;
                    case 0x6b:
                        _i[pc] = new MathOperation(JavaType.Double, MathOperation.Operator.Multiply);
                        break;
                    case 0x6c:
                        _i[pc] = new MathOperation(JavaType.Integer, MathOperation.Operator.Divide);
                        break;
                    case 0x6d:
                        _i[pc] = new MathOperation(JavaType.Long, MathOperation.Operator.Divide);
                        break;
                    case 0x6e:
                        _i[pc] = new MathOperation(JavaType.Float, MathOperation.Operator.Divide);
                        break;
                    case 0x6f:
                        _i[pc] = new MathOperation(JavaType.Double, MathOperation.Operator.Divide);
                        break;
                    case 0x70:
                        _i[pc] = new MathOperation(JavaType.Integer, MathOperation.Operator.Remainder);
                        break;
                    case 0x71:
                        _i[pc] = new MathOperation(JavaType.Long, MathOperation.Operator.Remainder);
                        break;
                    case 0x72:
                        _i[pc] = new MathOperation(JavaType.Float, MathOperation.Operator.Remainder);
                        break;
                    case 0x73:
                        _i[pc] = new MathOperation(JavaType.Double, MathOperation.Operator.Remainder);
                        break;
                    case 0x74:
                        _i[pc] = new Negate(JavaType.Integer);
                        break;
                    case 0x75:
                        _i[pc] = new Negate(JavaType.Long);
                        break;
                    case 0x76:
                        _i[pc] = new Negate(JavaType.Float);
                        break;
                    case 0x77:
                        _i[pc] = new Negate(JavaType.Double);
                        break;
                    case 0x78:
                        _i[pc] = new BitwiseOperation(JavaType.Integer, BitwiseOperation.Operator.ShiftLeft);
                        break;
                    case 0x79:
                        _i[pc] = new BitwiseOperation(JavaType.Long, BitwiseOperation.Operator.ShiftLeft);
                        break;
                    case 0x7a:
                        _i[pc] = new BitwiseOperation(JavaType.Integer, BitwiseOperation.Operator.ShiftRight);
                        break;
                    case 0x7b:
                        _i[pc] = new BitwiseOperation(JavaType.Long, BitwiseOperation.Operator.ShiftRight);
                        break;
                    case 0x7c:
                        _i[pc] = new BitwiseOperation(JavaType.Integer, BitwiseOperation.Operator.UnsignedShiftRight);
                        break;
                    case 0x7d:
                        _i[pc] = new BitwiseOperation(JavaType.Long, BitwiseOperation.Operator.UnsignedShiftRight);
                        break;
                    case 0x7e:
                        _i[pc] = new BitwiseOperation(JavaType.Integer, BitwiseOperation.Operator.And);
                        break;
                    case 0x7f:
                        _i[pc] = new BitwiseOperation(JavaType.Long, BitwiseOperation.Operator.And);
                        break;
                    case 0x80:
                        _i[pc] = new BitwiseOperation(JavaType.Integer, BitwiseOperation.Operator.Or);
                        break;
                    case 0x81:
                        _i[pc] = new BitwiseOperation(JavaType.Long, BitwiseOperation.Operator.Or);
                        break;
                    case 0x82:
                        _i[pc] = new BitwiseOperation(JavaType.Integer, BitwiseOperation.Operator.ExclusiveOr);
                        break;
                    case 0x83:
                        _i[pc] = new BitwiseOperation(JavaType.Long, BitwiseOperation.Operator.ExclusiveOr);
                        break;
                    case 0x84:
                        _i[pc] = new IInc(isWide ? disr.ReadUShort() : disr.ReadUByte(), isWide ? disr.ReadShort() : disr.ReadByte());
                        break;
                    #endregion

                    #region Conversions
                    case 0x85:
                        _i[pc] = new Convert(JavaType.Integer, JavaType.Long);
                        break;
                    case 0x86:
                        _i[pc] = new Convert(JavaType.Integer, JavaType.Float);
                        break;
                    case 0x87:
                        _i[pc] = new Convert(JavaType.Integer, JavaType.Double);
                        break;
                    case 0x88:
                        _i[pc] = new Convert(JavaType.Long, JavaType.Integer);
                        break;
                    case 0x89:
                        _i[pc] = new Convert(JavaType.Long, JavaType.Float);
                        break;
                    case 0x8a:
                        _i[pc] = new Convert(JavaType.Long, JavaType.Double);
                        break;
                    case 0x8b:
                        _i[pc] = new Convert(JavaType.Float, JavaType.Integer);
                        break;
                    case 0x8c:
                        _i[pc] = new Convert(JavaType.Float, JavaType.Long);
                        break;
                    case 0x8d:
                        _i[pc] = new Convert(JavaType.Float, JavaType.Double);
                        break;
                    case 0x8e:
                        _i[pc] = new Convert(JavaType.Double, JavaType.Integer);
                        break;
                    case 0x8f:
                        _i[pc] = new Convert(JavaType.Double, JavaType.Long);
                        break;
                    case 0x90:
                        _i[pc] = new Convert(JavaType.Double, JavaType.Float);
                        break;
                    case 0x91:
                        _i[pc] = new Convert(JavaType.Integer, JavaType.Byte);
                        break;
                    case 0x92:
                        _i[pc] = new Convert(JavaType.Integer, JavaType.Character);
                        break;
                    case 0x93:
                        _i[pc] = new Convert(JavaType.Integer, JavaType.Short);
                        break;
                    #endregion

                    #region Comparisons
                    case 0x94:
                        _i[pc] = new Compare(JavaType.Long, null);
                        break;
                    case 0x95:
                        _i[pc] = new Compare(JavaType.Float, Compare.ComparisonType.LessThan);
                        break;
                    case 0x96:
                        _i[pc] = new Compare(JavaType.Float, Compare.ComparisonType.GreaterThan);
                        break;
                    case 0x97:
                        _i[pc] = new Compare(JavaType.Double, Compare.ComparisonType.LessThan);
                        break;
                    case 0x98:
                        _i[pc] = new Compare(JavaType.Double, Compare.ComparisonType.GreaterThan);
                        break;
                    case 0x99:
                        _i[pc] = new If(If.Operator.Equal, disr.ReadShort());
                        break;
                    case 0x9a:
                        _i[pc] = new If(If.Operator.NotEqual, disr.ReadShort());
                        break;
                    case 0x9b:
                        _i[pc] = new If(If.Operator.LessThan, disr.ReadShort());
                        break;
                    case 0x9c:
                        _i[pc] = new If(If.Operator.GreaterThanOrEqual, disr.ReadShort());
                        break;
                    case 0x9d:
                        _i[pc] = new If(If.Operator.GreaterThan, disr.ReadShort());
                        break;
                    case 0x9e:
                        _i[pc] = new If(If.Operator.LessThanOrEqual, disr.ReadShort());
                        break;
                    case 0x9f:
                        _i[pc] = new BranchCompare(JavaType.Integer, BranchCompare.Operator.Equal, disr.ReadShort());
                        break;
                    case 0xa0:
                        _i[pc] = new BranchCompare(JavaType.Integer, BranchCompare.Operator.NotEqual, disr.ReadShort());
                        break;
                    case 0xa1:
                        _i[pc] = new BranchCompare(JavaType.Integer, BranchCompare.Operator.LessThan, disr.ReadShort());
                        break;
                    case 0xa2:
                        _i[pc] = new BranchCompare(JavaType.Integer, BranchCompare.Operator.GreaterThanOrEqual, disr.ReadShort());
                        break;
                    case 0xa3:
                        _i[pc] = new BranchCompare(JavaType.Integer, BranchCompare.Operator.GreaterThan, disr.ReadShort());
                        break;
                    case 0xa4:
                        _i[pc] = new BranchCompare(JavaType.Integer, BranchCompare.Operator.LessThanOrEqual, disr.ReadShort());
                        break;
                    case 0xa5:
                        _i[pc] = new BranchCompare(JavaType.Reference, BranchCompare.Operator.Equal, disr.ReadShort());
                        break;
                    case 0xa6:
                        _i[pc] = new BranchCompare(JavaType.Reference, BranchCompare.Operator.NotEqual, disr.ReadShort());
                        break;
                    #endregion

                    #region Control
                    case 0xa7:
                        _i[pc] = new Goto(disr.ReadShort());
                        break;
                    case 0xa8:
                        _i[pc] = new Jsr(disr.ReadShort());
                        break;
                    case 0xa9:
                        _i[pc] = new Ret(isWide ? disr.ReadUShort() : disr.ReadUByte());
                        break;
                    case 0xaa: //tableswitch
                        var pad = 4 - ((pc+1) % 4);
                        disr.Position += pad;
                        var def = disr.ReadInt();
                        var low = disr.ReadInt();
                        var hi = disr.ReadInt();
                        var l = (hi - low) + 1;
                        var dict = new Dictionary<int, int>();
                        for (var i = low; i <= hi; i++)
                            dict[i] = disr.ReadInt();
                        _i[pc] = new Switch(def, dict);
                        break;
                    case 0xab: // lookupswitch
                        pad = 4 - ((pc+1) % 4);
                        disr.Position += pad;
                        def = disr.ReadInt();
                        var npairs = disr.ReadInt();
                        dict = new Dictionary<int, int>();
                        for (var i = 0; i <= npairs; i++)
                            dict[disr.ReadInt()] = disr.ReadInt();
                        _i[pc] = new Switch(def, dict);
                        break;
                    case 0xac:
                        _i[pc] = new Return(JavaType.Integer);
                        break;
                    case 0xad:
                        _i[pc] = new Return(JavaType.Long);
                        break;
                    case 0xae:
                        _i[pc] = new Return(JavaType.Float);
                        break;
                    case 0xaf:
                        _i[pc] = new Return(JavaType.Double);
                        break;
                    case 0xb0:
                        _i[pc] = new Return(JavaType.Reference);
                        break;
                    case 0xb1:
                        _i[pc] = new Return(null);
                        break;
                    #endregion

                    #region References
                    case 0xb2:
                        _i[pc] = new GetStatic((ConstantFieldRef)cp[disr.ReadUShort()]);
                        break;
                    case 0xb3:
                        _i[pc] = new PutStatic((ConstantFieldRef)cp[disr.ReadUShort()]);
                        break;
                    case 0xb4:
                        _i[pc] = new GetField((ConstantFieldRef)cp[disr.ReadUShort()]);
                        break;
                    case 0xb5:
                        _i[pc] = new PutField((ConstantFieldRef)cp[disr.ReadUShort()]);
                        break;
                    case 0xb6:
                        _i[pc] = new InvokeVirtual((ConstantMethodRef)cp[disr.ReadUShort()]);
                        break;
                    case 0xb7:
                        _i[pc] = new InvokeSpecial((ConstantMethodRef)cp[disr.ReadUShort()]);
                        break;
                    case 0xb8:
                        _i[pc] = new InvokeStatic((ConstantMethodRef)cp[disr.ReadUShort()]);
                        break;
                    case 0xb9:
                        _i[pc] = new InvokeInterface((ConstantInterfaceMethodRef)cp[disr.ReadUShort()], disr.ReadUByte(), disr.ReadUByte());
                        break;
                    case 0xba:
                        _i[pc] = new InvokeDynamic((ConstantMethodHandle)cp[disr.ReadUShort()]);
                        disr.ReadShort();
                        break;
                    case 0xbb:
                        _i[pc] = new New((ConstantClass)cp[disr.ReadUShort()]);
                        break;
                    case 0xbc:
                        _i[pc] = new NewArray((NewArray.ArrayType)disr.ReadUByte());
                        break;
                    case 0xbd:
                        _i[pc] = new ANewArray((ConstantClass)cp[disr.ReadUShort()]);
                        break;
                    case 0xbe:
                        _i[pc] = new ArrayLength();
                        break;
                    case 0xbf:
                        _i[pc] = new AThrow();
                        break;
                    case 0xc0:
                        _i[pc] = new CheckCast((ConstantClass)cp[disr.ReadUShort()]);
                        break;
                    case 0xc1:
                        _i[pc] = new InstanceOf((ConstantClass)cp[disr.ReadUShort()]);
                        break;
                    case 0xc2:
                        _i[pc] = new MonitorEnter();
                        break;
                    case 0xc3:
                        _i[pc] = new MonitorExit();
                        break;
                    #endregion

                    #region Extended
                    case 0xc4:
                        isWide = true;
                        continue; // continue here so isWide is still true until end of next loop :)
                    case 0xc5:
                        _i[pc] = new MultiANewArray((ConstantClass)cp[disr.ReadUShort()], disr.ReadUByte());
                        break;
                    case 0xc6:
                        _i[pc] = new IfNull(disr.ReadShort());
                        break;
                    case 0xc7:
                        _i[pc] = new IfNonNull(disr.ReadShort());
                        break;
                    case 0xc8:
                        _i[pc] = new Goto(disr.ReadInt());
                        break;
                    case 0xc9:
                        _i[pc] = new Jsr(disr.ReadInt());
                        break;

                        #endregion
                }
                isWide = false;
            }
            #endregion

            // Collapse instructions into list
            var il = new InstructionList();
            var keys = _i.Keys;
            var prev = il.Root;
            foreach(var idx in keys)
            {
                var i = _i[idx];

                prev.Next = i;
                i.Previous = prev;
                prev = i;

                // Consolidate...
                if (!(i is BranchInstruction))
                    continue;
                var bi = (BranchInstruction) i;
                bi.Branch = _i[idx + ((uint) bi._branch)];

                if (!(i is Switch))
                    continue;

                var sw = (Switch) i;
                var jk = sw._jumpTable.Keys;
                foreach (var k in jk)
                    sw.JumpTable[k] = _i[idx + ((uint) sw._jumpTable[k])];
            }


            var exLen = disr.ReadUShort();
            var ex = new List<ExceptionTableEntry>();
            for (var i = 0; i < exLen; i++)
            {
                ex.Add(new ExceptionTableEntry
                            {
                                Start = _i[disr.ReadUShort()],
                                End = _i[disr.ReadUShort()],
                                Handler = _i[disr.ReadUShort()],
                                CatchType = (ConstantClass)cp[disr.ReadUShort()]
                            });
            }

            var cattr = new CodeAttribute
                                      {
                                          MaxStack = maxStack,
                                          MaxLocals = maxLocals,
                                          Code = il,
                                          ExceptionTable = ex,
                                          Attributes = new List<Attribute>()
                                      };

            var attrCount = disr.ReadUShort();
            var attr = new List<Attribute>();
            for (var i = 0; i < attrCount; i++)
            {
                attr.Add(ReadAttribute(cp, cattr, disr));
            }


            // Check for hidden data?
            if (disr.Position - init != len)
                throw new DataException();

            return cattr;
        }

        internal static Attribute ReadAttribute(ConstantPool cp, CodeAttribute cattr, DataInputStreamReader disr)
        {
            var anr = (ConstantUtf8)cp[disr.ReadUShort()];
            Attribute atr = null;
            switch (anr.Constant)
            {
                case "LineNumberTable":
                    atr = LineNumberTableAttribute.Read(cp, cattr, disr);
                    break;

                case "LocalVariableTable":
                    atr = LocalVariableTableAttribute.Read(cp, cattr, disr);
                    break;

                case "LocalVariableTypeTable":
                    atr = LocalVariableTypeTableAttribute.Read(cp, cattr, disr);
                    break;

                default:
                    if (anr.Constant.Equals("StackMapTable"))
                    {
                        anr.ToString();
                    }
                    Console.Error.WriteLine("Unhandled attribute: " + anr);
                    var len = disr.ReadUInt();
                    var data = new byte[len];
                    disr.ReadFully(data, 0, (int)len);
                    atr = new UnknownAttribute { Data = data };
                    break;
            }
            atr._nameRef = anr;
            return atr;
        }
    }

    public class ExceptionTableEntry
    {
        public Instruction Start { get; set; }
        public Instruction End { get; set; }
        public Instruction Handler { get; set; }
        public ConstantClass CatchType { get; set; }

        internal ExceptionTableEntry()
        {
        }
    }
}
