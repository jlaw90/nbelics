﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.instr;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class LocalVariableTableAttribute: Attribute
    {
        public List<LocalVariableTableEntry> LocalVariableTable { get; private set; } 

        internal LocalVariableTableAttribute()
        {
        }

        internal static LocalVariableTableAttribute Read(ConstantPool cp, CodeAttribute ca, DataInputStreamReader disr)
        {
            var len = disr.ReadInt();
            var count = disr.ReadUShort();
            var l = new List<LocalVariableTableEntry>();
            for(var i = 0; i < count; i++)
            {
                uint start = disr.ReadUShort();
                var end = start + disr.ReadUShort();
                l.Add(new LocalVariableTableEntry
                          {
                              Start = ca.Code.Offset(start),
                              End = ca.Code.Offset(end),
                              NameReference = (ConstantUtf8) cp[disr.ReadUShort()],
                              DescriptorReference = (ConstantUtf8) cp[disr.ReadUShort()],
                              Index = disr.ReadUShort()
                          });
            }

            return new LocalVariableTableAttribute {LocalVariableTable = l};
        }
    }

    public class LocalVariableTableEntry
    {
        public Instruction Start { get; set; }
        public Instruction End { get; set; }
        public ConstantUtf8 NameReference { get; set; }
        public string Name { get { return NameReference.Constant; } set { NameReference.Constant = value; } }
        public ConstantUtf8 DescriptorReference { get; set; }
        public string Descriptor { get { return DescriptorReference.Constant; } set { DescriptorReference.Constant = value; } }
        public ushort Index { get; set; }

        internal LocalVariableTableEntry()
        {
        }
    }
}
