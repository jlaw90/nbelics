﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NBELICS.io;
using NBELICS.model.pool;

namespace NBELICS.model.attr
{
    public class DeprecatedAttribute : Attribute
    {
        internal DeprecatedAttribute()
        {
        }

        internal new static DeprecatedAttribute Read(ConstantPool cp, DataInputStreamReader disr)
        {
            var len = disr.ReadUInt();
            if(len != 0)
                throw new DataException();
            return new DeprecatedAttribute();
        }
    }
}
