﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.attr
{
    public class StackMapTableAttribute
    {
        public List<StackMapFrame> Frames { get; private set; }
 
        internal StackMapTableAttribute()
        {
            Frames = new List<StackMapFrame>();
        }
    }

    public class StackMapFrame
    {
        
    }
}