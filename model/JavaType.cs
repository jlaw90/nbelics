﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model
{
    public enum JavaType
    {
        Reference,
        Byte,
        Boolean,
        Character,
        Double,
        Float,
        Integer,
        Long,
        Short
    }
}