﻿using System;
using System.Collections.Generic;
using NBELICS.model.attr;
using NBELICS.model.pool;
using Attribute = NBELICS.model.attr.Attribute;

namespace NBELICS.model
{
    public class Field
    {
        public AccessFlag AccessFlags { get; set; }
        public ConstantUtf8 NameReference { get; set; }
        public ConstantUtf8 DescriptorReference { get; set; }

        public string Name { get { return NameReference.Constant; } set { NameReference.Constant = value; } }
        public string Descriptor { get { return DescriptorReference.Constant; } set { DescriptorReference.Constant = value; } }
        public List<Attribute> Attributes { get; internal set; } 

        internal Field()
        {
            
        }
    }
}
