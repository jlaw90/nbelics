﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using NBELICS.io;
using NBELICS.model.pool;
using NBELICS.model.attr;
using Attribute = NBELICS.model.attr.Attribute;

namespace NBELICS.model
{
    public class ClassFile
    {
        public uint Magic { get; private set; }

        public ushort Minor { get; private set; }
        public ushort Major { get; private set; }

        public ConstantPool ConstantPool { get; private set; }

        //If the ACC_INTERFACE flag of this class file is set, its ACC_ABSTRACT flag must also be set (JLS §9.1.1.1). Such a class file must not have its ACC_FINAL, ACC_SUPER or ACC_ENUM flags set.
        //An annotation type must have its ACC_ANNOTATION flag set. If the ACC_ANNOTATION flag is set, the ACC_INTERFACE flag must be set as well. If the ACC_INTERFACE flag of this class file is not set, it may have any of the other flags in Table 4.1 set, except the ACC_ANNOTATION flag. However, such a class file cannot have both its ACC_FINAL and ACC_ABSTRACT flags set (JLS §8.1.1.2).
        public AccessFlag AccessFlags { get; set; }

        public static ClassFile Read(Stream s)
        {
            var disr = new DataInputStreamReader(s);

            var magic = disr.ReadUInt();

            if(magic != 0xCAFEBABE)
                throw new DataException("Invalid class file");

            var minor = disr.ReadUShort();
            var major = disr.ReadUShort();

            // Read the constant pool...
            var cp = ConstantPool.Read(disr);

            var flags = (AccessFlag) disr.ReadUShort();

            var thisClass = (ConstantClass) cp[disr.ReadUShort()];

            var superClass = (ConstantClass) cp[disr.ReadUShort()];

            var ifacecount = disr.ReadUShort();
            var ifaces = new List<ConstantClass>();

            for (var i = 0; i < ifacecount; i++ )
            {
                ifaces.Add((ConstantClass) cp[disr.ReadUShort()]);
            }

            var fieldcount = disr.ReadUShort();
            var fields = new List<Field>();

            for (var i = 0; i < fieldcount; i++ )
            {
                var faf = (AccessFlag) disr.ReadUShort();
                var nr = (ConstantUtf8) cp[disr.ReadUShort()];
                var dr = (ConstantUtf8) cp[disr.ReadUShort()];
                var attrs = new List<Attribute>();

                var fac = disr.ReadUShort();
                for(var j = 0; j < fac; j++)
                    attrs.Add(Attribute.Read(cp, disr));

                fields.Add(new Field{AccessFlags = faf, DescriptorReference = dr, NameReference = nr, Attributes = attrs});
            }

            var methodcount = disr.ReadUShort();
            var methods = new List<Method>();

            for (var i = 0; i < methodcount; i++)
            {
                var maf = (AccessFlag) disr.ReadUShort();
                var nr = (ConstantUtf8)cp[disr.ReadUShort()];
                var dr = (ConstantUtf8)cp[disr.ReadUShort()];
                var attrs = new List<Attribute>();

                var fac = disr.ReadUShort();
                for (var j = 0; j < fac; j++)
                    attrs.Add(Attribute.Read(cp, disr));

                methods.Add(new Method { AccessFlags = maf, DescriptorReference = dr, NameReference = nr, Attributes = attrs });
            }

            var attrcount = disr.ReadUShort();
            var cattrs = new List<Attribute>();

            for (var i = 0; i < attrcount; i++)
                cattrs.Add(Attribute.Read(cp, disr));


                return null;
        }
    }
}