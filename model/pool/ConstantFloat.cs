﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantFloat : ConstantValueEntry
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.Float; }
        }
        public new float Constant { get { return (float) base.Constant; } set { base.Constant = value; } }

        internal ConstantFloat(float f)
        {
            Constant = f;
        }

        public override string ToString()
        {
            return Constant.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is ConstantFloat && Equals((ConstantFloat)obj);
        }

        public bool Equals(ConstantFloat other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Constant == Constant;
        }

        public static ConstantFloat Create(ConstantPool cp, float f)
        {
            return cp.Insert(new ConstantFloat(f));
        }
    }
}
