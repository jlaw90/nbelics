﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantInteger: ConstantValueEntry
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.Integer; }
        }
        public int Constant { get { return (int)base.Constant; } set { base.Constant = value; } }

        internal ConstantInteger(int i)
        {
            Constant = i;
        }

        public override string ToString()
        {
            return Constant.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is ConstantInteger && Equals((ConstantInteger)obj);
        }

        public bool Equals(ConstantInteger other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Constant,Constant );
        }

        public static ConstantInteger Create(ConstantPool cp, int i)
        {
            return cp.Insert(new ConstantInteger(i));
        }
    }
}