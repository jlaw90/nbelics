﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantInterfaceMethodRef : ConstantFieldOrMethodRef
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.InterfaceMethodRef; }
        }

        internal ConstantInterfaceMethodRef()
        {
        }

        private ConstantInterfaceMethodRef(ConstantClass clazz, ConstantNameAndType nat): base(clazz, nat)
        {
            
        }

        public override string ToString()
        {
            return "InterfaceMethod" + base.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is ConstantInterfaceMethodRef && Equals((ConstantInterfaceMethodRef)obj);
        }

        public bool Equals(ConstantInterfaceMethodRef other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other);
        }

        public static ConstantInterfaceMethodRef Create(ConstantPool cp, string className, string name, string descriptor)
        {
            return Create(cp, className, name, descriptor, new ConstantInterfaceMethodRef());
        }
    }
}