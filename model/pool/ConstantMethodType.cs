﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantMethodType: ConstantEntry
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.MethodType; }
        }
        public ConstantUtf8 DescriptorReference { get; set; }
        public string Descriptor { get { return DescriptorReference.Constant; } set { DescriptorReference.Constant = value; } }
        internal ushort _didx;

        internal ConstantMethodType()
        {
        }

        private ConstantMethodType(ConstantUtf8 desc)
        {
            DescriptorReference = desc;
        }

        public override string ToString()
        {
            return "MethodType["+DescriptorReference + "]";
        }

        public static ConstantMethodType Create(ConstantPool cp, string descriptor)
        {
            return cp.Insert(new ConstantMethodType(ConstantUtf8.Create(cp, descriptor)));
        }
    }
}