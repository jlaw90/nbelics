﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
//reference_kind
//The value of the reference_kind item must be in the range 1 to 9. The value denotes the kind of this method handle, which characterizes its bytecode behavior (§5.4.3.5).
//
//reference_index
//The value of the reference_index item must be a valid index into the constant_pool table.
//If the value of the reference_kind item is 1 (REF_getField), 2 (REF_getStatic), 3 (REF_putField), or 4 (REF_putStatic), then the constant_pool entry at that index must be a CONSTANT_Fieldref_info (§4.4.2) structure representing a field for which a method handle is to be created.
//If the value of the reference_kind item is 5 (REF_invokeVirtual), 6 (REF_invokeStatic), 7 (REF_invokeSpecial), or 8 (REF_newInvokeSpecial), then the constant_pool entry at that index must be a CONSTANT_Methodref_info (§4.4.2) structure representing a class's method or constructor (§2.9) for which a method handle is to be created.
//If the value of the reference_kind item is 9 (REF_invokeInterface), then the constant_pool entry at that index must be a CONSTANT_InterfaceMethodref_info (§4.4.2) structure representing an interface's method for which a method handle is to be created.
//If the value of the reference_kind item is 5 (REF_invokeVirtual), 6 (REF_invokeStatic), 7 (REF_invokeSpecial), or 9 (REF_invokeInterface), the name of the method represented by a CONSTANT_Methodref_info structure must not be <init> or <clinit>.
//If the value is 8 (REF_newInvokeSpecial), the name of the method represented by a CONSTANT_Methodref_info structure must be <init>.
    public class ConstantMethodHandle: ConstantEntry
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.MethodHandle; }
        }
        public ConstantFieldOrMethodRef Reference { get; set; }
        public string Name { get { return Reference.Name; } set { Reference.Name = value; } }
        public string ClassName { get { return Reference.ClassName; } set { Reference.ClassName = value; } }
        public string Descriptor { get { return Reference.Descriptor; } set { Reference.Descriptor = value; } }
        internal byte _initKind;
        internal ushort _refIdx;

        internal ConstantMethodHandle()
        {
        }

        public override string ToString()
        {
            return "MethodHandle[Ref="+Reference+",Type="+_initKind+"]";
        }

        // Todo: add factory creation
        //public static ConstantMethodHandle Create(ConstantPool cp)
        //{
        //    var cmh = new ConstantMethodHandle();
        //}
    }
}