﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.io;

namespace NBELICS.model.pool
{
    public class ConstantPool
    {
        private readonly Dictionary<ushort, ConstantEntry> _entries;

        private ConstantPool()
        {
            _entries = new Dictionary<ushort, ConstantEntry>();
        }

        public ConstantEntry this[ushort i]
        {
            get { return i == 0? null: _entries[i]; }
            private set { _entries[i] = value; }
        }

        public ushort IndexOf(ConstantEntry ce)
        {
            try
            {
                return _entries.First(c => c.Value == ce).Key;
            } catch(Exception e)
            {
                return 0;
            }
        }

        public T Insert<T>(T ce) where T : ConstantEntry
        {
            // Check if it already exists in this constantpool
            var j = IndexOf(ce);
            if (j != 0)
                return ce;

            // Find a space
            ushort idx;
            for (ushort i = 0; true; i++)
            {
                if (_entries[i] == null)
                {
                    idx = i;
                    break;
                }
                if (_entries[i].Tag == ConstantTag.Long || _entries[i].Tag == ConstantTag.Double)
                    i++;
            }

            _entries[idx] = ce;
            ce.Pool = this;
            return ce;
        }

        public bool Remove(ConstantEntry ce)
        {
            return _entries.Remove(_entries.Single(e =>
                                                       {
                                                           if (e.Value == ce)
                                                           {
                                                               e.Value.Pool = null;
                                                               return true;
                                                           }
                                                           return false;
                                                       }).Key);
        }

        internal void ReIndex()
        {
            var ce = _entries.Values.ToList();
            _entries.Clear();
            ushort i = 1;
            foreach (var e in ce)
            {
                _entries[i++] = e;
                if (e.Tag == ConstantTag.Long || e.Tag == ConstantTag.Double)
                    i++;
            }
        }

        static internal ConstantPool Read(DataInputStreamReader disr)
        {
            var count = disr.ReadUShort();
            var cp = new ConstantPool();

            for (ushort i = 1; i < count; i++)
            {
                var tag = disr.ReadUByte();
                ConstantEntry n;

                switch ((ConstantTag)tag)
                {
                    case ConstantTag.Utf8:
                        n = new ConstantUtf8(disr.ReadUTF());
                        break;
                    case ConstantTag.Integer:
                        n = new ConstantInteger(disr.ReadInt());
                        break;
                    case ConstantTag.Float:
                        n = new ConstantFloat(disr.ReadFloat());
                        break;
                    case ConstantTag.Long:
                        n = new ConstantLong(disr.ReadLong());
                        break;
                    case ConstantTag.Double:
                        n = new ConstantDouble(disr.ReadDouble());
                        break;
                    case ConstantTag.String:
                        n = new ConstantString { _validx = disr.ReadUShort() };
                        break;
                    case ConstantTag.Class:
                        n = new ConstantClass { _validx = disr.ReadUShort() };
                        break;
                    case ConstantTag.NameAndType:
                        n = new ConstantNameAndType { _nameidx = disr.ReadUShort(), _descidx = disr.ReadUShort() };
                        break;
                    case ConstantTag.FieldRef:
                        n = new ConstantFieldRef { _cidx = disr.ReadUShort(), _natidx = disr.ReadUShort() };
                        break;
                    case ConstantTag.MethodRef:
                        n = new ConstantMethodRef { _cidx = disr.ReadUShort(), _natidx = disr.ReadUShort() };
                        break;
                    case ConstantTag.InterfaceMethodRef:
                        n = new ConstantInterfaceMethodRef { _cidx = disr.ReadUShort(), _natidx = disr.ReadUShort() };
                        break;
                    case ConstantTag.MethodHandle:
                        n = new ConstantMethodHandle { _initKind = disr.ReadUByte(), _refIdx = disr.ReadUShort() };
                        break;
                    case ConstantTag.MethodType:
                        n = new ConstantMethodType { _didx = disr.ReadUShort() };
                        break;
                    case ConstantTag.InvokeDynamic:
                        n = new ConstantInvokeDynamic { _bootstrapidx = disr.ReadUShort(), _natidx = disr.ReadUShort() };
                        break;
                    default:
                        throw new NotSupportedException("This class file has an entry in the constant pool that this library does not recognise.");
                        break;
                }
                cp[i] = n;
                n.Pool = cp;
                if (n.Tag == ConstantTag.Double || n.Tag == ConstantTag.Long)
                    i++;
            }

            // Consolidate the constant pool
            foreach (var ce in cp._entries.Select(e => e.Value))
            {
                switch (ce.Tag)
                {
                    case ConstantTag.Class:
                        var cc = (ConstantClass)ce;
                        cc.NameReference = (ConstantUtf8)cp[cc._validx];
                        break;
                    case ConstantTag.FieldRef:
                    case ConstantTag.MethodRef:
                    case ConstantTag.InterfaceMethodRef:
                        var cr = (ConstantFieldOrMethodRef)ce;
                        cr.Class = (ConstantClass)cp[cr._cidx];
                        cr.NameAndType = (ConstantNameAndType)cp[cr._natidx];
                        break;

                    case ConstantTag.InvokeDynamic:
                        var cid = (ConstantInvokeDynamic)ce;
                        cid.NameAndType = (ConstantNameAndType)cp[cid._natidx];
                        break;
                    case ConstantTag.MethodHandle:
                        var cme = (ConstantMethodHandle)ce;
                        cme.Reference = (ConstantFieldOrMethodRef)cp[cme._refIdx];
                        break;
                    case ConstantTag.MethodType:
                        var cmt = (ConstantMethodType)ce;
                        cmt.DescriptorReference = (ConstantUtf8)cp[cmt._didx];
                        break;
                    case ConstantTag.NameAndType:
                        var cnat = (ConstantNameAndType)ce;
                        cnat.DescriptorReference = (ConstantUtf8)cp[cnat._descidx];
                        cnat.NameReference = (ConstantUtf8)cp[cnat._nameidx];
                        break;
                    case ConstantTag.String:
                        var cs = (ConstantString)ce;
                        cs.StringReference = (ConstantUtf8)cp[cs._validx];
                        break;
                }
            }

            return cp;
        }
    }
}