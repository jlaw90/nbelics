﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantMethodRef: ConstantFieldOrMethodRef
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.MethodRef; }
        }

        internal ConstantMethodRef()
        {
        }

        private ConstantMethodRef(ConstantClass clazz, ConstantNameAndType nat): base(clazz, nat)
        {
            
        }

        public override string ToString()
        {
            return "Method" + base.ToString();
        }

        public static ConstantMethodRef Create(ConstantPool cp, string className, string name, string descriptor)
        {
            return Create(cp, className, name, descriptor, new ConstantMethodRef());
        }
    }
}