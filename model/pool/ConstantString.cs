﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantString: ConstantValueEntry
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.String; }
        }
        public ConstantUtf8 StringReference { get; set; }
        public new string Constant { get { return StringReference.Constant; } set { StringReference.Constant = value; } }
        internal ushort _validx;

        internal ConstantString()
        {
        }

        public ConstantString(ConstantUtf8 str)
        {
            StringReference = str;
        }

        public override string ToString()
        {
            return "\"" + StringReference + "\"";
        }

        public static ConstantString Create(ConstantPool cp, string str)
        {
            return cp.Insert(new ConstantString(ConstantUtf8.Create(cp, str)));
        }
    }
}