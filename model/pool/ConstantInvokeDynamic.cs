﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantInvokeDynamic: ConstantEntry
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.InvokeDynamic; }
        }
        public ConstantNameAndType NameAndType { get; set; }
        // Todo: link to reference in bootstrap method array...
        internal ushort _bootstrapidx, _natidx;

        public string Name { get { return NameAndType.Name; } set { NameAndType.Name = value; } }
        public string Descriptor { get { return NameAndType.Descriptor; } set { NameAndType.Descriptor = value; } }

        internal ConstantInvokeDynamic()
        {
        }

        /*public ConstantInvokeDynamic(ConstantNameAndType cnat)
        {
            NameAndType = cnat;
        }*/

        public override string ToString()
        {
            return "InvokeDynamic[Type="+NameAndType+",bootstrap="+_bootstrapidx+"]";
        }
    }
}