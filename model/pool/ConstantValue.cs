﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public abstract class ConstantValueEntry: ConstantEntry
    {
        protected object Constant { get; set; }
    }
}