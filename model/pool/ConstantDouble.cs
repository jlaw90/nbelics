﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantDouble : ConstantValueEntry
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.Double; }
        }
        public double Constant { get { return (double) base.Constant; } set { base.Constant = value; } }

        internal ConstantDouble(double d)
        {
            this.Constant = d;
        }

        public override string ToString()
        {
            return Constant.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is ConstantDouble && Equals((ConstantDouble)obj);
        }

        public bool Equals(ConstantDouble other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Constant == Constant;
        }

        public static ConstantDouble Create(ConstantPool cp, double d)
        {
            return cp.Insert(new ConstantDouble(d));
        }
    }
}
