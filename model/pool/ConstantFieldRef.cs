﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantFieldRef: ConstantFieldOrMethodRef
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.FieldRef; }
        }

        public override string ToString()
        {
            return "Field" + base.ToString();
        }

        private ConstantFieldRef(ConstantClass clazz, ConstantNameAndType nat): base(clazz, nat)
        {
            
        }

        internal ConstantFieldRef()
        {
        }

        public override bool Equals(object obj)
        {
            return obj is ConstantFieldRef && Equals((ConstantFieldRef)obj);
        }

        public bool Equals(ConstantFieldRef other)
        {
            if (ReferenceEquals(other, null))
                return false;
            return base.Equals(other);
        }

        public static ConstantFieldRef Create(ConstantPool cp, string className, string name, string descriptor)
        {
            return Create(cp, className, name, descriptor, new ConstantFieldRef());
        }
    }
}
