﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantClass: ConstantEntry
    {
        public override ConstantTag Tag { get { return ConstantTag.Class; } }
        public ConstantUtf8 NameReference { get; set; }

        public string Name { get { return NameReference.Constant; } set { NameReference.Constant = value; } }

        internal ushort _validx;

        internal ConstantClass (){}

        private ConstantClass(ConstantUtf8 name)
        {
            NameReference = name;
        }

        public override string ToString()
        {
            return "class(" + NameReference + ")";
        }

        public override bool Equals(object obj)
        {
            return obj.GetType() == typeof(ConstantClass) && Equals((ConstantClass)obj);
        }

        public bool Equals(ConstantClass other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.NameReference, NameReference);
        }

        public static ConstantClass Create(ConstantPool cp, string name)
        {
            return cp.Insert(new ConstantClass(ConstantUtf8.Create(cp, name)));
        }
    }
}