﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantLong : ConstantValueEntry
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.Long; }
        }
        public long Constant { get { return (long)base.Constant; } set { base.Constant = value; } }

        internal ConstantLong(long l)
        {
            Constant = l;
        }

        public override string ToString()
        {
            return Constant.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is ConstantLong && Equals((ConstantLong)obj);
        }

        public bool Equals(ConstantLong other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Constant == Constant;
        }

        public static ConstantLong Create(ConstantPool cp, long l)
        {
            return cp.Insert(new ConstantLong(l));
        }
    }
}
