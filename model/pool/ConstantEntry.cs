﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public abstract class ConstantEntry
    {
        public abstract ConstantTag Tag { get; }
        public ConstantPool Pool { get; internal set; }

        public ushort Index { 
            get
            {
                if(Pool == null)
                    throw new InvalidOperationException("This constant is not currently attached to a constant pool");
                return Pool.IndexOf(this);
            }
        }
    }
}