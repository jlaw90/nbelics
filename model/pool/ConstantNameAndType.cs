﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantNameAndType: ConstantEntry
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.NameAndType; }
        }
        public ConstantUtf8 NameReference { get; set; }
        public ConstantUtf8 DescriptorReference { get; set; }

        public string Name { get { return NameReference.Constant; } set { NameReference.Constant = value; } }
        public string Descriptor { get { return DescriptorReference.Constant; } set { DescriptorReference.Constant = value; } }

        internal ushort _nameidx, _descidx;

        internal ConstantNameAndType()
        {
        }

        private ConstantNameAndType(ConstantUtf8 name, ConstantUtf8 descriptor)
        {
            this.NameReference = name;
            this.DescriptorReference = descriptor;
        }

        public override string ToString()
        {
            return "Ref[Name=" + NameReference + ",Descriptor=" + DescriptorReference + "]";
        }

        public static ConstantNameAndType Create(ConstantPool cp, string name, string descriptor)
        {
            return cp.Insert(new ConstantNameAndType(ConstantUtf8.Create(cp, name), ConstantUtf8.Create(cp, descriptor)));
        }
    }
}