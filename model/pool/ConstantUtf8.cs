﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public class ConstantUtf8: ConstantValueEntry
    {
        public override ConstantTag Tag
        {
            get { return ConstantTag.Utf8; }
        }
        public string Constant { get { return (string)base.Constant; } set { base.Constant = value; } }

        internal ConstantUtf8(string s)
        {
            Constant = s;
        }

        public override string ToString()
        {
            return Constant;
        }

        public static ConstantUtf8 Create(ConstantPool cp, string str)
        {
            return cp.Insert(new ConstantUtf8(str));
        }
    }
}