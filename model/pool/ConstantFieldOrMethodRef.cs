﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model.pool
{
    public abstract class ConstantFieldOrMethodRef: ConstantEntry
    {
        public ConstantClass Class { get; set; }
        public ConstantNameAndType NameAndType { get; set; }

        public string ClassName { get { return Class.Name; } set { Class.Name = value; } }
        public string Name { get { return NameAndType.Name; } set { NameAndType.Name = value; } }
        public string Descriptor { get { return NameAndType.Descriptor; } set { NameAndType.Descriptor = value; } }

        internal ushort _natidx, _cidx;

        internal ConstantFieldOrMethodRef()
        {
        }

        protected ConstantFieldOrMethodRef(ConstantClass clazz, ConstantNameAndType cnat)
        {
            Class = clazz;
            NameAndType = cnat;
        }

        public override string ToString()
        {
            return "Ref[Class=" + Class + ",Type=" + NameAndType + "]";
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null))
                return false;
            return obj.GetType() == GetType() && Equals((ConstantFieldOrMethodRef)obj);
        }

        public bool Equals(ConstantFieldOrMethodRef other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Class, Class) && Equals(other.NameAndType, NameAndType);
        }

        internal static T Create<T>(ConstantPool cp, string className, string name, string descriptor, T cfomr) where T: ConstantFieldOrMethodRef
        {
            cfomr.NameAndType = ConstantNameAndType.Create(cp, name, descriptor);
            cfomr.Class = ConstantClass.Create(cp, className);
            return cp.Insert(cfomr);
        }
    }
}