﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NBELICS.model
{
    [Flags]
    public enum AccessFlag
    {
        Public = 0x1,
        Private = 0x2,
        Protected = 0x4,
        Static = 0x8,
        Final = 0x10,
        Synchronized = 0x20,
        Super = 0x20,
        Bridge = 0x40,
        Volatile = 0x40,
        VarArgs = 0x80,
        Transient = 0x80,
        Native = 0x100,
        Interface = 0x200,
        Abstract = 0x400,
        Strict = 0x800,
        Synthetic = 0x1000,
        Annotation = 0x2000,
        Enum = 0x4000
    }
}