﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBELICS.model.attr;
using NBELICS.model.pool;
using Attribute = NBELICS.model.attr.Attribute;

namespace NBELICS.model
{
    public class Method
    {
        public AccessFlag AccessFlags { get; set; }
        public ConstantUtf8 NameReference { get; set; }
        public ConstantUtf8 DescriptorReference { get; set; }

        public string Name { get { return NameReference.Constant; } set { NameReference.Constant = value; } }
        public string Descriptor { get { return DescriptorReference.Constant; } set { DescriptorReference.Constant = value; } }
        public List<Attribute> Attributes { get; internal set; }

        // Just to save time
        public CodeAttribute Code { get { return (CodeAttribute) GetAttribute("Code"); } }

        internal Method()
        {
            
        }

        public Attribute GetAttribute(string name)
        {
            return Attributes.FirstOrDefault(a => a.Name.Equals(name));
        }
    }
}
